import axios from 'axios';

const base = '/api/module2';

export default {
  getConsultantRecord() {
    return axios.get(`${base}/consultant_records`);
  },
  getAllConsultantRecord() {
    return axios.get(`${base}/consultants`);
  },
  getFilterConsultantRecord() {
    return axios.get(`${base}/filter_consultant_record`);
  },
  getConsultantRecordByID(id) {
    return axios.get(`${base}/consultant_record`, id);
  },
  postConsultantRecord(data) {
    return axios.post(`${base}/consultant_record`, data);
  },
};
