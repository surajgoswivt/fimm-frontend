import axios from 'axios';

const base = 'http://localhost:7002/api/module2';

export default {
  getAllTermination() {
    return axios.get(`${base}/getAllTermination`);
  },
  postTermination(data) {
    return axios.post(`${base}/consultant_termination`, data);
  },
};
