import axios from 'axios';

const base = 'http://localhost:7002/api/module2';

export default {
  postResignation(data) {
    return axios.post(`${base}/consultant_resignation`, data);
  },
};
