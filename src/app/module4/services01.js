// Nurul - post & pre

import axios from 'axios';
import Vue from 'vue'
import * as servicesModule0 from '../module0/services';


export async function getCompanyId(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/company_record", {
    params: {
      USER_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function getProgramById(data, category, type) {
  await servicesModule0.header();
  return axios.get("/api/module4/program_record", {
    params: {
      COMPANY_ID: data,
      CATEGORY: category,
      PROG_TYPE: type,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function filterRecordList(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/filter_record', { params: data }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        logout();
      }
      return 'error';
    }
  });
}

//cpd Details
export async function getEventByProgramId(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/program_details_record", {
    params: {
      PROGRAM_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

// get details program by distributor
export async function getProgramByDistId(data,category, type) {
  await servicesModule0.header();
  return axios.get("/api/module4/Program_DetailsByDist", {
    params: {
      COMPANY_ID: data,
      CATEGORY: category,
      PROG_TYPE: type,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function checkRunNo(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/dist_runNumber", {
    params: {
      DISTRIBUTOR_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}
//get All
export async function getAllDeliveryMode() {
  await servicesModule0.header();
  return axios.get("/api/module4/delivery_mode").then(response => {
    console.log("Delivery Mode list :" + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}
export async function getAllProgramType() {
  await servicesModule0.header();
  return axios.get("/api/module4/program_type").then(response => {
    console.log("Program Type list :" + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}
export async function getAllMethod() {
  await servicesModule0.header();
  return axios.get("/api/module4/slo_method").then(response => {
    console.log("Slo Method list :" + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}
export async function getAllMedia() {
  await servicesModule0.header();
  return axios.get("/api/module4/slo_media").then(response => {
    console.log("Slo Media list :" + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}
export async function getAllAssessment() {
  await servicesModule0.header();
  return axios.get("/api/module4/slo_assessment").then(response => {
    console.log("Slo Method list :" + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function getSlo(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/slo_record', {
    params: {
      SLO_LIST: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}
export async function getParticipant(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/participant_record', {
    params: {
      PARTICIPANT_LIST: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}
export async function getFeedback(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/feedback_record', {
    params: {
      FEEDBACK_LIST: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}

export async function createNewPostVet(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/create_newPostVet", data).then(response => {
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

// get list of distributor
export async function getProgramByDist(category, type) {
  await servicesModule0.header();
  return axios.get("/api/module4/program_byDist", {
    params: {
      CATEGORY: category,
      PROG_TYPE: type,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function getDetailsByID(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/program_details_byId", {
    params: {
      PROG_DETAILS_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}
export async function getNewDetailsByID(data,status) {
  await servicesModule0.header();
  return axios.get("/api/module4/repeatprogram_details_byId", {
    params: {
      PROGRAM_ID: data,
      PROG_DETAILS_REPEAT: status,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}
export async function getSloByID(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/slo_recordById', {
    params: {
      PROG_DETAILS_ID: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}

export async function getFeedbackByID(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/feedback_recordById', {
    params: {
      PROG_DETAILS_ID: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}

export async function getProgramDetailsID(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/get_ProgramDetailsId', {
    params: {
      PROGRAM_ID: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}


export async function updateApproval(data) {
  await servicesModule0.header()
  return axios.post('/api/module4/Program_approval_update', data).then(response => {
    // console.log("exception create :" + JSON.stringify(response.data));
    return response.data
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function returnSubmission(data) {
  await servicesModule0.header()
  return axios.post('/api/module4/return_submission', data).then(response => {
    // console.log("exception create :" + JSON.stringify(response.data));
    return response.data
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function importExcelPrevet(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/import_pre_vetting", data).then(response => {
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function importExcelPostVet(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/import_post_vetting", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });

    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function importExcelPostVetUpdate(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/update_post_vetting", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });

    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}


//get document blob
export async function getDocumentById(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/get_document', {
    params: {
      PROG_DETAILS_ID: data,
    },
  }).then(response => {
    console.log("Dococument list : " + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          //logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        //logout();
      }
      return 'error';
    }
  });
}


//delete doc

export async function deleteDocument(data) {
  await servicesModule0.header();
  return axios.delete('/api/module4/delete_document', {
    params: {
      PROG_DOCUMENT_ID: data,
    },
  })
    .then(response => {
      console.log('data :' + JSON.stringify(response.data));
      return response.data;
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == 'Token expired.') {
            logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          logout();
        }
      }
    });
}
export async function deleteSlo(data) {
  await servicesModule0.header();
  return axios.delete('/api/module4/delete_slo', {
    params: {
      PROG_SLO_ID: data,
    },
  })
    .then(response => {
      console.log('data :' + JSON.stringify(response.data));
      return response.data;
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == 'Token expired.') {
            logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          logout();
        }
      }
    });
}

//edit SLo
export async function updateSlo(data) {
  await servicesModule0.header()
  return axios.post('/api/module4/slo_update', data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data
  }).catch(function (error) {
    if (error.response) {
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function getSloByRow(data) {
  await servicesModule0.header();
  return axios.get('/api/module4/slo_recordBySloId', {
    params: {
      PROG_SLO_ID: data
    },
  }).then(response => {
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout');
          servicesModule2.logout();
        } else {
          console.log(error.response.data.message);
        }
      } else if (error.response.status == 401) {
        servicesModule2.logout();
      }
      return 'error';
    }
  });
}

export async function createSlo(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/create_slo", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function cancelProgram(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/cancel_program", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function updateProgram(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/update_program", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function updateRepeatedPrevet(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/update_repeatedPrevet", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function getAuditLogByID(data) {
  await servicesModule0.header()
  return axios.get('/api/module4/audit_log', {
      params: {
          PROG_DETAILS_ID: data,
      },
  }).then(response => {
      console.log('data :' + JSON.stringify(response.data.data))
      return response.data.data
  }).catch(function(error) {
      if (error.response) {
          Vue.$toast.open({
            message: error.response.data.message,
            type: 'error',
          })
          if (error.response.status == 401) {
              logout()
          } else {
              return 'error'
          }
      }
  })
}

export async function createRepeatedPrevetting(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/repeated_prevetting", data).then(response => {
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function getParticipantByID(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/get_participantByID", {
    params: {
      PROG_DETAILS_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function getRejectedListByID(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/get_rejectedByID", {
    params: {
      PROG_DETAILS_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {

      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}


export async function deleteParticipant(data) {
  await servicesModule0.header();
  return axios.delete('/api/module4/delete_participant', {
    params: {
      PROG_PARTICIPANT_ID: data,
    },
  })
    .then(response => {
      console.log('data :' + JSON.stringify(response.data));
      return response.data;
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == 'Token expired.') {
            logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          logout();
        }
      }
    });
}


export async function getCountPrevetDistributor() {
  await servicesModule0.header()
  return axios.get('/api/module4/get_CountPrevetDistributor').then(response => {
      console.log('data :' + JSON.stringify(response.data.data))
      return response.data.data
  }).catch(function(error) {
      if (error.response) {
          Vue.$toast.open({
            message: error.response.data.message,
            type: 'error',
          })
          if (error.response.status == 401) {
              logout()
          } else {
              return 'error'
          }
      }
  })
}

export async function updatePostVet(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/update_postVetting", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      // Vue.$toast.open({
      //   message: error.response.data.message,
      //   type: 'error',
      // });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function createFb(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/create_fb", data).then(response => {
    // Vue.$toast.open({
    //   message: response.data.message,
    //   type: 'success',
    // });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function updateFb(data) {
  await servicesModule0.header()
  return axios.post('/api/module4/fb_update', data).then(response => {
    // Vue.$toast.open({
    //   message: response.data.message,
    //   type: 'success',
    // });
    return response.data
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function addParticipant(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/add_participant", data).then(response => {
     Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });

    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

//********************************************waiver************************************************************** */

export async function getWaiverByDistID(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/waiver_record_byDistID", {
    params: {
      DISTRIBUTOR_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function uploadNewWaiver(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/import_new_waiver", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });

    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function getWaiverListByWaiverID(data) {
  await servicesModule0.header()
  return axios.get('/api/module4/get_listByWaiverID', {
    params: {
      WAIVER_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data))
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function getWaiverRejectedListByID(data) {
  await servicesModule0.header();
  return axios.get("/api/module4/get_waiverRejectedByID", {
    params: {
      WAIVER_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function discardWaiverRecord(data) {
  await servicesModule0.header();
  return axios.delete('/api/module4/discard_waiver_record', {
    params: {
      WAIVER_ID: data,
    },
  })
    .then(response => {
      console.log('data :' + JSON.stringify(response.data));
      return response.data;
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == 'Token expired.') {
            logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          logout();
        }
      }
    });
}

export async function submitWaiverRecord(data) {
  await servicesModule0.header();
  return axios.post("/api/module4/submit_waiver_record", data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data;
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      });
      if (error.response.status == 401) {
        logout();
      } else {
        return "error"
      }
    }
  });
}

export async function deleteConsultantWaiver(data) {
  await servicesModule0.header();
  return axios.delete('/api/module4/delete_record_waiver', {
    params: {
      WAIVER_PARTICIPANT_ID: data,
    },
  })
    .then(response => {
      console.log('data :' + JSON.stringify(response.data));
      return response.data;
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == 'Token expired.') {
            logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          logout();
        }
      }
    });
}

// upload support document
export async function uploadWaiverDocument(data) {
  let config = {
    header : {
      'Content-Type' : 'multipart/form-data'
    }
  }
  await servicesModule0.header()
  return axios.post('/api/module4/upload_waiver_document', data, config)
  .then(response => {
    Vue.$toast.open({
      message: 'Successfully support document uploaded.',
      type: 'success',
    })
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

//approval
export async function getWaiverListByDist() {
  await servicesModule0.header();
  return axios.get("/api/module4/waiver_byDist").then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function getWaiverSubmissionList() {
  await servicesModule0.header();
  return axios.get("/api/module4/waiver_submissionList").then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data;
  }).catch(function (error) {
    if (error.response) {
      if (error.response.status == 401) {
        logout();
      } else {
        return 'error'
      }
    }
  });
}

export async function updateWaiverSubmission(data) {
  await servicesModule0.header()
  return axios.post('/api/module4/update_waiver_submission', data).then(response => {
    Vue.$toast.open({
      message: response.data.message,
      type: 'success',
    });
    return response.data
  }).catch(function (error) {
    if (error.response) {
      Vue.$toast.open({
        message: error.response.data.message,
        type: 'error',
      })
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function getWaiverAuditLogByID(data) {
  await servicesModule0.header()
  return axios.get('/api/module4/audit_waiver_record', {
    params: {
      WAIVER_ID: data,
    },
  }).then(response => {
    console.log('data :' + JSON.stringify(response.data.data))
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      if (error.response.status == 401) {
        logout()
      } else {
        return 'error'
      }
    }
  })
}