/* eslint-disable no-unused-vars */
// acap
import axios from 'axios';
import Vue from 'vue';

// Distributor list 

export async function getDistributorList() {
  await this.header();
  return axios.get('/api/module1/distributor_users').then(response => {
          console.log('data :' + JSON.stringify(response.data.data));
          return response.data.data;
      }).catch(function(error) {
          if (error.response) {
              Vue.$toast.open({
                  message: error.response.data.message,
                  type: 'error',
              });
              if (error.response.status === 401) {
                  logout();
              } else {
                  return 'error';
              }
          }
      });
}

export async function getDistUserInfo(data) {
  return axios.get('/api/module1/getDistUserInfo',{
            params: {
              USER_ID: data
          },
          }).then(response => {
          console.log('data :' + JSON.stringify(response.data.data));
          return response.data.data;
      }).catch(function(error) {
          if (error.response) {
            if (error.response.status == 500) {
              // console.log(JSON.stringify(error.response.data.message));
              if (error.response.data.message == 'Token expired.') {
                console.log('logout');
                logout();
              } else {
                console.log(error.response.data.message);
              }
            } else if (error.response.status == 401) {
              logout();
            }
            return 'error';
          }
      });
}

export async function getDistributor() {
  return axios.get('/api/module1/getDistributor').then(response => {
          console.log('data :' + JSON.stringify(response.data.data));
          return response.data.data;
      }).catch(function(error) {
          if (error.response) {
            if (error.response.status == 500) {
              // console.log(JSON.stringify(error.response.data.message));
              if (error.response.data.message == 'Token expired.') {
                console.log('logout');
                logout();
              } else {
                console.log(error.response.data.message);
              }
            } else if (error.response.status == 401) {
              logout();
            }
            return 'error';
          }
      });
}
// export async function getDistributorList () {
//   await this.header();
//   return axios.get('/api/module1/distributor_users').then(response => {
//     console.log('data :' + JSON.stringify(response.data.data));
//     return response.data.data;
//   }).catch(function (error){
//     if (error.response) {
//       if (error.response.status == 500) {
//         // console.log(JSON.stringify(error.response.data.message));
//         if (error.response.data.message == 'Token expired.') {
//           console.log('logout');
//           logout();
//         } else {
//           console.log(error.response.data.message);
//         }
//       } else if (error.response.status == 401) {
//         logout();
//       }
//       return 'error';
//     }
//   });
// }

// roles assign

export async function getMemberRoles () {
  await this.header();
  return axios.get('/api/module1/setting_postal').then(response => {
    console.log('data :' + JSON.stringify(response.data.data));
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout')
          logout()
        } else {
          console.log(error.response.data.message)
        }
      } else if (error.response.status == 401) {
        logout()
      }
      return 'error'
    }
  })
}
