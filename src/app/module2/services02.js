//

import axios from 'axios';
import Vue from 'vue'
import * as servicesModule0 from '../module0/services';

export async function postRegisterConsultant(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/consultant", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}
export async function updateRegisterConsultant(data) {
  await servicesModule0.header();
  return axios
    .put(`/api/module2/consultant/${data.CONSULTANT_ID}`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getConsultant() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/consultant_records")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}
export async function getAllConsultant() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/consultants")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}
export async function getConsultantByID(data) {
  await servicesModule0.header();
  return axios
    .get("/api/module2/consultant_record", {
      responseType: "application/json",
      params: {
        CONSULTANT_ID: data
      }
    })
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getDistributorRecords() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/distributor_records")
    .then(response => {
      // console.log('data :' + JSON.stringify(response));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getConsultantByDistributorID(id) {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/distributor/${id}`)
    .then(response => {
      console.log("data :" + JSON.stringify(response));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getAllTermination() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/getAllTermination")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}
export async function getTerminationType() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/termination_type")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postTerminateConsultant(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/consultant_termination", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function updateTerminateConsultant(data) {
  await servicesModule0.header();
  return axios.post(`/api/module2/consultant_termination`, data).then(response => {
    console.log('list : ' + JSON.stringify(response.data));
    return response.data;
  });
}

export async function updateTerminateConsultantApproval(data) {
   await servicesModule0.header();
  return axios.post(`/api/module2/bulk_termination_approval`, data)
    .then(response => {
    console.log('list : ' + JSON.stringify(response.data));
        Vue.$toast.open({
          message: response.data.message,
          type: 'success',
        });
       return response.data.data;
    })
    .catch(function (error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: 'error',
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return 'error';
        }
      }
    });;
}

export async function postResignConsultant(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/consultant_resignation", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putResignConsultant(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/consultant_resignation", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getConsultantResignation() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/consultant_resignation`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postResignApprovalConsultant(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/consultant_resignation_approval", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putResignApprovalConsultant(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/consultant_resignation_approval", data)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getConsultantResignationApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/consultant_resignation_approval`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function registrationStatus() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/registration_status")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function updateSelfReg(data) {
  return axios
    .post("/api/module2/update_register", data)
    .then(response => {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == "Token expired.") {
            console.log("logout");
            servicesModule1.logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          servicesModule1.logout();
        }
        return "error";
      }
    });
}

export async function verifyUser(data) {
  return axios
    .get("/api/module2/verify_user", {
      params: {
        LOGIN_ID: data.LOGIN_ID,
        USER_PASS_NUM: data.USER_PASS_NUM
      }
    })
    .then(response => {
      // localStorage.setItem('realmName', 'realm1');

      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        if (error.response.status == 500) {
          // console.log(JSON.stringify(error.response.data.message));
          if (error.response.data.message == "Token expired.") {
            console.log("logout");
            servicesModule1.logout();
          } else {
            console.log(error.response.data.message);
          }
        } else if (error.response.status == 401) {
          servicesModule1.logout();
        }
        return "error";
      }
    });
}
export async function getPaymentStatus() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/payment_status")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getTerminationByConsultantID(id) {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/termination/${id}`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getDistributorTerminationList() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/getDistributorTermination`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getDistributorApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/getDistributorApproval`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getConsultantPendingApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/getConsultantPendingApproval`)
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getTerminationByDistributor(data) {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/getTerminationByDistributor/${data}`)
    .then(response => {
      console.log("list : " + JSON.stringify(response.data.data));
      return response.data.data;
    });
}

export async function getConsultantsByDistributor(data) {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/getConsultantsByDistributor/${data}`)
    .then(response => {
      console.log("list : " + JSON.stringify(response.data.data));
      return response.data.data;
    });
}

export async function postAppeal(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/appeal", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putAppeal(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/appeal", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getAppeal() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/appeal`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postAppealApproval(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/appeal_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putAppealApproval(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/appeal_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getAppealApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/appeal_approval`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postRenewal(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/renewal", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putRenewal(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/renewal", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getRenewal() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/renewal`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postRenewalApproval(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/renewal_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putRenewalApproval(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/renewal_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getRenewalApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/renewal_approval`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postBankruptcy(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/bankruptcy", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putBankruptcy(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/bankruptcy", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getBankruptcy() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/bankruptcy`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function postBankruptcyApproval(data) {
  await servicesModule0.header();
  return axios
    .post("/api/module2/bankruptcy_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function putBankruptcyApproval(data) {
  await servicesModule0.header();
  return axios
    .put("/api/module2/bankruptcy_approval", data)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

export async function getBankruptcyApproval() {
  await servicesModule0.header();
  return axios
    .get(`/api/module2/bankruptcy_approval`)
    .then(response => {
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        Vue.$toast.open({
          message: error.response.data.message,
          type: "error"
        });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}

//get distributor wallet
export async function getDistributorWallet() {
  await servicesModule0.header();
  return axios
    .get("/api/module2/distributorWallets")
    .then(response => {
      // console.log('data :' + JSON.stringify(response.data.data));
      return response.data.data;
    })
    .catch(function(error) {
      console.log(error);
      if (error.response) {
        // Vue.$toast.open({
        //   message: error.response.data.message,
        //   type: 'error',
        // });
        if (error.response.status == 401) {
          logout();
        } else {
          return "error";
        }
      }
    });
}
