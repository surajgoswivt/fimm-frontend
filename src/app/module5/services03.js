// acap
import axios from 'axios';
import Vue from 'vue';
import * as servicesModule0 from '../module0/services';
import { logout } from '../module1/services03';

import * as servicesModule5 from "./services";

export async function getDistributorList() {
    return axios.get('/api/module5/getDistributor_list')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}

//get document blob
export async function getDocumentById(data) {
    // await servicesModule0.header();
    console.log(data);
    return axios.get('/api/module5/get_document', {
        params: {
            FundProfileID: data,
        },
    }).then(response => {
        console.log("Document list : " + JSON.stringify(response.data.data));
        return response.data.data;
    }).catch(function(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    //logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                //logout();
            }
            return 'error';
        }
    });
}


//delete doc

export async function deleteDocument(data) {
    await servicesModule0.header();
    return axios.delete('/api/module5/delete_document', {
            params: {
                FundProfileID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data));
            return response.data;
        }).catch(function(error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout();
                    } else {
                        console.log(error.response.data.message);
                    }
                } else if (error.response.status == 401) {
                    logout();
                }
            }
        });
}
// 1. fund creation ( distributor )
export async function createFundProfile(data) {
    return axios.post('/api/module5/fund_profile', data).then(response => {
        console.log(JSON.stringify(response.data));
        return response.data;
    }).catch(function(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    //logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                //logout();
            }
            return 'error';
        }
    });
}
// 2. fund creation get data (distributor )

export async function getRunningNoFundCode() {
    return axios.get('/api/module5/getRunningNoFundCode')
        .then(response => {
            console.log(JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        console.log('logout');
                        //logout();
                    } else {
                        console.log(error.response.data.message);
                    }
                } else if (error.response.status == 401) {
                    //logout();
                }
                return 'error';
            }
        });
}

export async function updateRunningNoFundCode(data) {
    return axios
        .post('/api/module5/updateRunningNoFundCode', data)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout();
                    } else {
                        console.log(error.response.data.message)
                    }
                } else if (error.response.status == 401) {
                    logout();
                }
            }
        });
}

export async function getFundProfileInfo(data) {
    return axios.get('/api/module5/getFundProfileInfo', {
            params: {
                FUND_PROFILE_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}


export async function getAllCurrencyFormat() {
    return axios.get('/api/module5/getAllCurrency')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}

export async function getFundProfile(data) {
    return axios.get('/api/module5/fund_profile', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function getFundProfileActive(data) {
    return axios.get('/api/module5/fund_profile_active', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}


export async function getFundProfileReviewFIMM(data) {
    return axios.get('/api/module5/fund_profile_review_FIMM', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}

export async function getFundProfileReview(data) {
    return axios.get('/api/module5/fund_profile_review', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function getFundProfileReviewDetails(data) {
    return axios.get('/api/module5/fund_profile_review2', {
            params: {
                FUND_PROFILE_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}

export async function updateApprovalFundCreation(data) {
    return axios
        .post('/api/module5/fund_creation_approvalFM', data)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout()
                    } else {
                        console.log(error.response.data.message)
                    }
                } else if (error.response.status == 401) {
                    logout()
                }
            }
        })
}
export async function updateApprovalFundCreationFiMM(data) {
    return axios
        .post('/api/module5/fund_creation_approvalFiMM', data)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout()
                    } else {
                        console.log(error.response.data.message)
                    }
                } else if (error.response.status == 401) {
                    logout()
                }
            }
        })
}
// fund Update

export async function updateFundProfile(data) {
    const jsonObject = {}

    for (const [key, value] of data) {
        jsonObject[key] = value
    }
    return axios
        .put('/api/module5/fund_profile_update', jsonObject)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data))
            return response.data
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout()
                    } else {
                        console.log(error.response.data.message)
                    }
                } else if (error.response.status == 401) {
                    logout()
                }
            }
        })
}

// 3.fund lodgement create
export async function createFundLodgement(data) {
    await servicesModule0.header()
    return axios.post('/api/module5/fund_lodge_creation', data).then(response => {
        console.log(JSON.stringify(response.data));
        return response.data;
    }).catch(function(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    //logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                //logout();
            }
            return 'error';
        }
    });
}

// 4. fund lodgement get data
export async function getAllFundLodgement() {
    return axios.get('/api/module5/fund_lodge')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data))
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout()
                } else {
                    return 'error'
                }
            }
        })
}

export async function getFundLodgementList() {
    return axios.get('/api/module5/getfund_lodgeList')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data))
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout()
                } else {
                    return 'error'
                }
            }
        })
}
export async function getFPLodge() {
    return axios.get('/api/module5/getFPLodge')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data))
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout()
                } else {
                    return 'error'
                }
            }
        })
}

// 5. fund Application ( Deletion / Suspension / Closure)
export async function createFundApplication(data) {
    await servicesModule0.header();
    return axios.post('/api/module5/fund_application_creation', data).then(response => {
        return response.data.data;
    }).catch(function(error) {
        if (error.response) {
            console.log('error : ' + error.response)
            if (error.response.status == 500) {
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                logout();
            }
            return 'error';
        }
    });
}

export async function fund_application_status(data) {
    return axios.get('/api/module5/fund_application_status', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.respose.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function fund_application_fimm() {
    return axios.get('/api/module5/fund_application_fimm')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.respose.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function getAllFundApplication(data) {
    return axios.get('/api/module5/fund_application', {
            params: {
                DIST_ID: data,
            },
        })
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function getAllFundApplicationJoin() {
    return axios.get('/api/module5/fund_application_get')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        });
}
export async function updateApprovalFundApplication(data) {
    return axios
        .post('/api/module5/fund_application_approvalFM', data)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout();
                    } else {
                        console.log(error.response.data.message);
                    }
                } else if (error.response.status == 401) {
                    logout();
                }
            }
        });
}
export async function updateApprovalFundApplicationFiMM(data) {
    return axios
        .post('/api/module5/fund_application_approvalFiMM', data)
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                });
                if (error.response.status == 500) {
                    // console.log(JSON.stringify(error.response.data.message));
                    if (error.response.data.message == 'Token expired.') {
                        logout();
                    } else {
                        console.log(error.response.data.message);
                    }
                } else if (error.response.status == 401) {
                    logout();
                }
            }
        });
}
// Fund Management
export async function getAllFundTempData() {
    return axios.get('/api/module5/fund_temp')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data))
            return response.data.data
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout()
                } else {
                    return 'error'
                }
            }
        })
}

// 3rd party company registration
export async function createTPUser(data) {
    return axios.post('/api/module5/others_reg', data).then(response => {
        return response.data
    }).catch(function(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    servicesModule5.logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                servicesModule5.logout();
            }
            return 'error';
        }
    })
}
export async function createTRPProfile(data) {
    return axios.post('/api/module5/createTRPProfile', data).then(response => {
        return response.data
    }).catch(function(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout');
                    servicesModule5.logout();
                } else {
                    console.log(error.response.data.message);
                }
            } else if (error.response.status == 401) {
                servicesModule5.logout();
            }
            return 'error';
        }
    })
}
export async function getAllTPRegData() {
    return axios.get('/api/module5/getAllTPCompany').then(response => {
        console.log('data :' + JSON.stringify(response.data.data))
        return response.data.data
    }).catch(function(error) {
        if (error.response) {
            Vue.$toast.open({
                message: error.response.data.message,
                type: 'error',
            })
            if (error.response.status === 401) {
                logout()
            } else {
                return 'error'
            }
        }
    })
}
// NAV Daily Publish

// NAV Daily Update

// NAV Management

// nav lIST
export async function getNAVList() {
    return axios.get('/api/module5/getNAVLIST')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data))
            return response.data.data
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout()
                } else {
                    return 'error'
                }
            }
        })
}


// without middleware
export async function getFundProfileFilter() {
    return axios.get('/api/module5/fundlistMedia')
        .then(response => {
            console.log('data :' + JSON.stringify(response.data.data));
            return response.data.data;
        }).catch(function(error) {
            if (error.response) {
                Vue.$toast.open({
                    message: error.response.data.message,
                    type: 'error',
                })
                if (error.response.status === 401) {
                    logout();
                } else {
                    return 'error';
                }
            }
        })
}

export async function filterFundDistributorList(data) {
    await servicesModule0.header()
    return axios.get('/api/module5/filterFundDistributorList', { params: data }).then(response => {
        console.log('data :' + JSON.stringify(response.data.data))
        return response.data.data
    }).catch(function(error) {
        if (error.response) {
            if (error.response.status == 500) {
                // console.log(JSON.stringify(error.response.data.message));
                if (error.response.data.message == 'Token expired.') {
                    console.log('logout')
                    logout()
                } else {
                    console.log(error.response.data.message)
                }
            } else if (error.response.status == 401) {
                logout()
            }
            return 'error'
        }
    })
}