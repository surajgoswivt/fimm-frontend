// ros

import axios from 'axios'
import * as servicesModule7 from '../module0/services'

// R&A
export async function getFundList () {
  await servicesModule7.header()
  return axios.get('/api/module7/fundsubmission').then(response => {
    console.log('Record list :' + JSON.stringify(response.data.data))
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 401) {
        // logout()
      } else {
        return 'error'
      }
    }
  })
}

//filter
export async function filter_submission_record(data) {
  await servicesModule7.header()
  return axios.get('/api/module7/filter_submission_record', { params: data }).then(response => {
      console.log('data :' + JSON.stringify(response.data.data))
      return response.data.data
  }).catch(function(error) {
      if (error.response) {
          // Vue.$toast.open({
          //   message: error.response.data.message,
          //   type: 'error',
          // })
          if (error.response.status == 500) {
              // console.log(JSON.stringify(error.response.data.message));
              if (error.response.data.message == 'Token expired.') {
                  console.log('logout')
                  logout()
              } else {
                  console.log(error.response.data.message)
              }
          } else if (error.response.status == 401) {
              logout()
          }
          return 'error'
      }
  })
}

export async function update_submission (data) {
  await servicesModule7.header()
  return axios.post('/api/module7/update_submission', data).then(response => {
    // console.log("exception create :" + JSON.stringify(response.data));
    return response.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 401) {
        // logout()
      } else {
        return 'error'
      }
    }
  })
}

export async function returnFund (data) {
  await servicesModule7.header()
  return axios.post('/api/module7/returnFund', data).then(response => {
    // console.log("exception create :" + JSON.stringify(response.data));
    return response.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 401) {
        // logout()
      } else {
        return 'error'
      }
    }
  })
}

// aum submission


export async function getaumdetails (data) {
  await servicesModule7.header()
  return axios.get('/api/module7/getaumdetails', {
    params: {
      USER_DIST_ID: data,
    },
  }).then(response => {
    console.log(JSON.stringify(response.data.data))
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 500) {
        // console.log(JSON.stringify(error.response.data.message));
        if (error.response.data.message == 'Token expired.') {
          console.log('logout')
          // logout();
        } else {
          console.log(error.response.data.message)
        }
      } else if (error.response.status == 401) {
        // logout();
      }
      return 'error'
    }
  })
}




//finance
export async function getFundSummaryList () {
  await servicesModule7.header()
  return axios.get('/api/module7/getFundSummaryList').then(response => {
    console.log('Record list :' + JSON.stringify(response.data.data))
    return response.data.data
  }).catch(function (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status == 401) {
        // logout()
      } else {
        return 'error'
      }
    }
  })
}

//hod finance