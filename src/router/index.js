import Vue from 'vue';
import Router from 'vue-router';
import AuthLayout from '../components/auth/AuthLayout';
import AuthLayoutDistributor from '../components/auth/AuthLayoutDistributor';
import AuthLayoutDistributorRecoverPass from '../components/auth/AuthLayoutRecoverPassword';
import AuthLayoutOthers from '@/components/auth/AuthLayoutOthers';
import AuthLayoutOthersRecoverPass from '../components/auth/AuthLayoutRecoverPassword';
import AppLayout from '../components/admin/AppLayout';
import AppLayoutconsultant from '../components/consultant/AppLayout';
import defaultPage from '../components/auth/default.vue';
import fmspage from '../components/fms/landingpage';
import fmspageloggedin from '../components/fms/landingpagesignin';
import * as services from '../app/module0/services';
import ConsultantRoutes from './consultant';

Vue.use(Router);

// Global event object for cross components events emitter
window.Event = new Vue();

const EmptyParentComponent = {
    template: '<router-view></router-view>',
};

const ifLogin = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next();
    } else if (JSON.parse(localStorage.getItem('user')).user_type === 'fimm') {
        next('fimm/dashboard');
    } else if (JSON.parse(localStorage.getItem('user')).user_type === 'distributor') {
        next('distributor/dashboard');
    } else if (JSON.parse(localStorage.getItem('user')).user_type === 'consultant') {
        next('consultant/dashboard');
    }
};

const ifAuthenticatedFiMM = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next('login');
    } else {
        console.log(localStorage.getItem('user'));
        next();
    }
};

const ifAuthenticatedDistributor = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next('login-distributor');
    } else {
        next();
    }
};

const ifAuthenticatedConsultant = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next('login-consultant');
    } else {
        next();
    }
};

const ifAuthenticatedThirdParty = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next('login-third-party');
    } else {
        next();
    }
};

const ifAuthenticatedTrainingProvider = (to, from, next) => {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === 'undefined') {
        next('login-training-provider');
    } else {
        next();
    }
};

const demoRoutes = [];

export default new Router({
    // mode: process.env.VUE_APP_ROUTER_MODE_HISTORY === 'true' ? 'history' : 'hash',
    mode: 'history',
    routes: [
        ...demoRoutes,
        {
            path: '*',
            redirect: { name: 'default' },
            meta: {
                progress: {
                    func: [
                        { call: 'color', modifier: 'temp', argument: '#ffb000' },
                        { call: 'fail', modifier: 'temp', argument: '#6e0000' },
                        { call: 'location', modifier: 'temp', argument: 'top' },
                        {
                            call: 'transition',
                            modifier: 'temp',
                            argument: { speed: '1.5s', opacity: '0.6s', termination: 400 },
                        },
                    ],
                },
            },
            beforeEnter: ifLogin,
        },
        {
            name: 'fms-website',
            path: '/fms-website',
            component: fmspage,
            props: true,
            beforeEnter: ifLogin,
        },
        {
            name: 'default',
            path: '/default',
            component: defaultPage,
            beforeEnter: ifLogin,
        },
        {
            path: '/auth-others',
            component: AuthLayoutOthers,
            beforeEnter: ifLogin,
            children: [{
                    name: 'login-others',
                    path: 'login-others',
                    component: () =>
                        import ('../components/auth/login/Login-others.vue'),
                }, , {
                    name: 'signup-others',
                    path: 'signup-others',
                    component: () =>
                        import ('../components/auth/signup/Signup-others.vue'),
                },
                {
                    name: 'admin-registration',
                    path: 'admin-registration',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdparty_admin_registration.vue'),
                    props: true,
                },
            ],
        },
        {
            path: '/auth-others-recover',
            component: AuthLayoutOthersRecoverPass,
            children: [{
                name: 'others-recover-password',
                path: 'others-recover-password',
                component: () =>
                    import ('../components/auth/first-time-user/OthersFirstTimeUserResetPassword.vue'),
                props: true,
            }, ],
        },
        {
            path: '/auth-distributor',
            component: AuthLayoutDistributor,
            beforeEnter: ifLogin,
            meta: { requiresGuest: true },
            children: [{
                    name: 'login-distributor',
                    path: 'login-distributor',
                    beforeEnter: ifLogin,
                    meta: { requiresGuest: true },
                    component: () =>
                        import ('../components/auth/login/Login-distributor.vue'),
                    default: true,
                },
                {
                    name: 'signup-distributor',
                    path: 'signup-distributor',
                    meta: { requiresGuest: true },
                    beforeEnter: ifLogin,
                    component: () =>
                        import ('../components/auth/signup/Signup-distributor.vue'),
                },
                {
                    name: 'distributor-member-detailsRegistration',
                    path: 'distributor-member-detailsRegistration',
                    beforeEnter: ifLogin,
                    meta: { requiresGuest: true },
                    component: () =>
                        import ('../components/auth/signup/D-FirstTimeUserSignUp.vue'),
                },
            ],
        },
        {
            path: '/auth-distributor-recover',
            component: AuthLayoutDistributorRecoverPass,
            children: [{
                name: 'recover-password',
                path: 'recover-password',
                component: () =>
                    import ('../components/auth/recover-password/RecoverPassword.vue'),
                props: true,
            }, ],
        },
        {
            path: '/auth',
            component: AuthLayout,
            children: [{
                    name: 'login',
                    path: 'login',
                    component: () =>
                        import ('../components/auth/login/Login.vue'),
                    default: true,
                },
                {
                    name: 'signup',
                    path: 'signup',
                    component: () =>
                        import ('../components/auth/signup/Signup.vue'),
                },
            ],
        },
        {
            path: '/404',
            component: EmptyParentComponent,
            children: [{
                    name: 'not-found-advanced',
                    path: 'not-found-advanced',
                    component: () =>
                        import ('../components/pages/404-pages/VaPageNotFoundSearch.vue'),
                },
                {
                    name: 'not-found-simple',
                    path: 'not-found-simple',
                    component: () =>
                        import ('../components/pages/404-pages/VaPageNotFoundSimple.vue'),
                },
                {
                    name: 'not-found-custom',
                    path: 'not-found-custom',
                    component: () =>
                        import ('../components/pages/404-pages/VaPageNotFoundCustom.vue'),
                },
                {
                    name: 'not-found-large-text',
                    path: '/pages/not-found-large-text',
                    component: () =>
                        import ('../components/pages/404-pages/VaPageNotFoundLargeText.vue'),
                },
            ],
        },
        /* ---------------ADMIN PAGES--------------------- */
        {
            name: 'Admin',
            path: '/fimm',
            component: AppLayout,
            meta: {
                progress: {
                    func: [
                        { call: 'color', modifier: 'temp', argument: '#ffb000' },
                        { call: 'fail', modifier: 'temp', argument: '#6e0000' },
                        { call: 'location', modifier: 'temp', argument: 'top' },
                        {
                            call: 'transition',
                            modifier: 'temp',
                            argument: { speed: '1.5s', opacity: '0.6s', termination: 400 },
                        },
                    ],
                },
            },
            children: [{
                    name: 'dashboard',
                    path: 'dashboard',
                    component: () =>
                        import ('../components/dashboard/Dashboard.vue'),
                    default: true,
                },
                // {
                //   name: 'exam-report',
                //   path: 'exam-report',
                //   component: () => import('../pages/dummy1.vue')
                // },
                {
                    name: 'css',
                    path: 'css',
                    component: () =>
                        import ('../pages/demo/Icons_n_colors/icons_n_colors.vue'),
                },
                {
                    name: 'calendar-api',
                    path: 'calendar-api',
                    component: () =>
                        import ('../pages/dummy.vue'),
                },
                {
                    name: 'announcement',
                    path: 'announcement',
                    component: () =>
                        import ('../pages/admin-configuration/announcement/index.vue'),
                    props: { role: 'fimm' },
                },
                {
                    name: 'pending-task',
                    path: 'pending-task',
                    component: () =>
                        import ('../pages/admin-configuration/pending-task/index.vue'),
                    props: { role: 'fimm' },
                },
                {
                    name: 'notification',
                    path: 'notification',
                    component: () =>
                        import ('../pages/admin-configuration/notification/index.vue'),
                    props: { role: 'fimm' },
                },
                {
                    name: 'profile',
                    path: 'profile',
                    component: () =>
                        import ('../pages/admin-configuration/profile/index.vue'),
                    props: { role: 'fimm' },
                },
                // {
                //   name: 'company-register',
                //   path: 'company-register',
                //   component: () => import('../pages/distributor-management/cm1_CompanyRegister.vue'),
                //   props: true,
                // },
                // {
                //     name: 'distributor-details-registration',
                //     path: 'distributor-details-registration',
                //     component: () =>
                //         import ('../pages/distributor-management/distributor/cm1_distributor_detailsRegistration.vue'),
                // },
                // RD Annoucement Management
                {
                    name: 'event-list',
                    path: 'event-list',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/cm0_eventList.vue'),
                },
                {
                    name: 'new-event',
                    path: 'new-event',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/cm0_newEvent.vue'),
                },
                {
                    name: 'update-event',
                    path: 'update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/cm0_eventUpdate.vue'),
                },
                {
                    name: 'eventList-approval',
                    path: 'eventList-approval',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/cm0_eventApprovalList.vue'
                        ),
                },
                {
                    name: 'event-hod-approval',
                    path: 'event-hod-approval/:MANAGE_EVENT_ID',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/cm0_eventHodApproval.vue'),
                },
                {
                    name: 'event-approval-details',
                    path: 'event-approval-details/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/cm0_eventApprovalDetails.vue'
                        ),
                },
                // {
                //     name: 'circular-list',
                //     path: 'circular-list',
                //     component: () =>
                //         import ('../pages/admin-configuration/circular-management/cm0_circularList.vue'),
                // },
                // -- LRA Annoucement
                {
                    name: 'lra-event-list',
                    path: 'lra-event-list',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/LRA/cm0_eventList.vue'),
                },
                {
                    name: 'lra-new-event',
                    path: 'lra-new-event',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/LRA/cm0_newEvent.vue'),
                },
                {
                    name: 'lra-update-event',
                    path: 'lra-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/LRA/cm0_eventUpdate.vue'),
                },
                // -- industry DEv Annoucement
                {
                    name: 'id-event-list',
                    path: 'id-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/industry-department/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'id-new-event',
                    path: 'id-new-event',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/industry-department/cm0_newEvent.vue'
                        ),
                },
                {
                    name: 'id-update-event',
                    path: 'id-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/industry-department/cm0_eventUpdate.vue'
                        ),
                },
                // -- Finance Annoucement
                {
                    name: 'fin-event-list',
                    path: 'fin-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/finance/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'fin-new-event',
                    path: 'fin-new-event',
                    component: () =>
                        import ('../pages/admin-configuration/announcement-management/finance/cm0_newEvent.vue'),
                },
                {
                    name: 'fin-update-event',
                    path: 'fin-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/finance/cm0_eventUpdate.vue'
                        ),
                },
                // -- it-management Annoucement
                {
                    name: 'it-event-list',
                    path: 'it-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/it-management/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'it-new-event',
                    path: 'it-new-event',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/it-management/cm0_newEvent.vue'
                        ),
                },
                {
                    name: 'it-update-event',
                    path: 'it-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/it-management/cm0_eventUpdate.vue'
                        ),
                },
                // -- professional development Annoucement
                {
                    name: 'pd-event-list',
                    path: 'pd-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/professional-dev/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'pd-new-event',
                    path: 'pd-new-event',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/professional-dev/cm0_newEvent.vue'
                        ),
                },
                {
                    name: 'pd-update-event',
                    path: 'pd-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/professional-dev/cm0_eventUpdate.vue'
                        ),
                },
                // -- Rna Annoucement
                {
                    name: 'rna-event-list',
                    path: 'rna-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/research-and-analystic/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'rna-new-event',
                    path: 'rna-new-event',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/research-and-analystic/cm0_newEvent.vue'
                        ),
                },
                {
                    name: 'rna-update-event',
                    path: 'rna-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/research-and-analystic/cm0_eventUpdate.vue'
                        ),
                },
                // -- supervision Annoucement
                {
                    name: 'supervision-event-list',
                    path: 'supervision-event-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/supervision/cm0_eventList.vue'
                        ),
                },
                {
                    name: 'supervision-new-event',
                    path: 'supervision-new-event',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/supervision/cm0_newEvent.vue'
                        ),
                },
                {
                    name: 'supervision-update-event',
                    path: 'supervision-update-event/:MANAGE_EVENT_ID',
                    component: () =>
                        import (
                            '../pages/admin-configuration/announcement-management/supervision/cm0_eventUpdate.vue'
                        ),
                },
                //-------------------
               
                //RD circular - NISA
                {
                    name: 'rd-circular-List',
                    path: 'rd-circular-List',
                    component: () =>
                        import ('../pages/admin-configuration/circular-management/registrationDepartment/cm0_circularList.vue'),
                },
                {
                    name: 'rd-new-circular',
                    path: 'rd-new-circular',
                    component: () =>
                        import ('../pages/admin-configuration/circular-management/registrationDepartment/cm0_newCircular.vue'),
                },
                {
                    name: 'rd-circular-update',
                    path: 'rd-circular-update/:id',
                    component: () =>
                        import ('../pages/admin-configuration/circular-management/registrationDepartment/cm0_circularUpdate.vue'),
                },
                {
                    name: 'Hodrd-circular-List',
                    path: 'Hodrd-circular-List',
                    component: () =>
                        import (
                            '../pages/admin-configuration/circular-management/registrationDepartment/cm0_circularHodApprovalList.vue'
                        ),
                },
                // RD CIRCULAR END - NISA
                //LRA CIRCULAR - NISA
                {
                    name: 'lra-circular-List',
                    path: 'lra-circular-List',
                    component: () =>
                        import ('../pages/admin-configuration/circular-management/LegalDepartment/cm0_lra_CircularList.vue'),
                },
                //LRA CIRCULAR END -NISA
                {
                    name: 'excel-template',
                    path: 'excel-template',
                    component: () =>
                        import ('../pages/admin-configuration/document-management/cm0_excelTemplate.vue'),
                },
                {
                    name: 'document-checkList',
                    path: 'document-checkList',
                    component: () =>
                        import ('../pages/admin-configuration/document-management/cm0_documentChecklist.vue'),
                },
                {
                    name: 'calendar-management',
                    path: 'calendar-management',
                    component: () =>
                        import ('../pages/admin-configuration/calendar-management/cm0_calendarManagement.vue'),
                },
                {
                    name: 'address-management',
                    path: 'address-management',
                    component: () =>
                        import ('../pages/admin-configuration/address-management/cm0_addressManagement.vue'),
                    meta: {
                        progress: {
                            func: [
                                { call: 'color', modifier: 'temp', argument: '#ffb000' },
                                { call: 'fail', modifier: 'temp', argument: '#6e0000' },
                                { call: 'location', modifier: 'temp', argument: 'top' },
                                {
                                    call: 'transition',
                                    modifier: 'temp',
                                    argument: { speed: '1.5s', opacity: '0.6s', termination: 400 },
                                },
                            ],
                        },
                    },
                },
                {
                    name: 'sms-notification',
                    path: 'sms-notification',
                    component: () =>
                        import ('../pages/admin-configuration/sms-notification/cm0_smsNotifcation.vue'),
                },
                {
                    name: 'salutation-setting',
                    path: 'salutation-setting',
                    component: () =>
                        import ('../pages/admin-configuration/general-setting/salutation-setting.vue'),
                },
                {
                    name: 'gender-setting',
                    path: 'gender-setting',
                    component: () =>
                        import ('../pages/admin-configuration/general-setting/gender.vue'),
                },
                {
                    name: 'currency-setting',
                    path: 'currency-setting',
                    component: () =>
                        import ('../pages/admin-configuration/general-setting/currency.vue'),
                },
                {
                    name: 'login-setting',
                    path: 'login-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/login-setting.vue'),
                },
                {
                    name: 'password-setting',
                    path: 'password-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/password-setting.vue'),
                },
                {
                    name: 'user-id-setting',
                    path: 'user-id-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/user-id-setting.vue'),
                },
                {
                    name: 'screen-management-setting',
                    path: 'screen-management-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/screen-management-setting.vue'),
                },
                {
                    name: 'other-access-setting',
                    path: 'other-access-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/other-access-setting.vue'),
                },
                {
                    name: 'integration-setting',
                    path: 'integration-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/integration-setting.vue'),
                },
                {
                    name: 'address-setting',
                    path: 'address-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/address-setting.vue'),
                },
                {
                    name: 'general-setting',
                    path: 'general-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/general-setting.vue'),
                },
                {
                    name: 'calendar-setting',
                    path: 'calendar-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/calendar-setting.vue'),
                },
                {
                    name: 'announcement-setting',
                    path: 'announcement-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/announcement-setting.vue'),
                },
                {
                    name: 'circular-setting',
                    path: 'circular-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/circular-setting.vue'),
                },
                {
                    name: 'bymodule-list',
                    path: 'bymodule-list',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/bymodule-list.vue'),
                },
                {
                    name: 'document-form-setting',
                    path: 'document-form-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/document-form-setting.vue'),
                },
                {
                    name: 'page-maintanance-setting',
                    path: 'page-maintanance-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/page-maintanance-setting.vue'),
                },
                {
                    name: 'fee-management-setting',
                    path: 'fee-management-setting',
                    component: () =>
                        import ('../pages/admin-configuration/system-setting/fee-management-setting.vue'),
                },
                {
                    name: 'finance-main-setting',
                    path: 'finance-main-setting',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-finance/cm0_finance_mainSetting.vue'),
                },
                {
                    name: 'finance-account-setting',
                    path: 'finance-account-setting',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-finance/cm0_finance_account_setting.vue'),
                },
                {
                    name: 'email-notification',
                    path: 'email-notification',
                    component: () =>
                        import ('../pages/admin-configuration/Email-Management/email-notification.vue'),
                },
                {
                    name: 'ldap-configuration',
                    path: 'ldap-configuration',
                    component: () =>
                        import ('../pages/admin-configuration/LDAP-Management/cm0_ldapConfiguration.vue'),
                },
                {
                    name: 'finance-configuration',
                    path: 'finance-configuration',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-finance/cm0_finance_mainSetting.vue'),
                },
                {
                    name: 'distributor-configuration',
                    path: 'distributor-configuration',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byModule-distributor/cm0_distributor_mainSetting.vue'
                        ),
                },
                {
                    name: 'distributor-type',
                    path: 'distributor-type',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-distributor/distributor-type.vue'),
                },
                {
                    name: 'consultant-configuration',
                    path: 'consultant-configuration',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byModule-consultant/cm0_consultant_mainSetting.vue'
                        ),
                },
                {
                    name: 'cpd-configuration',
                    path: 'cpd-configuration',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-CPD/cpd-main-setting.vue'),
                },
                {
                    name: 'annualFee-configuration',
                    path: 'annualFee-configuration',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-annualFee/cm0_annualFee_mainSetting.vue'),
                },
                {
                    name: 'annualFee-invoice-configuration',
                    path: 'annualFee-invoice-configuration',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byModule-annualFee/annualFee-invoice-configuration.vue'
                        ),
                },
                {
                    name: 'fundMalaysia-configuration',
                    path: 'fundMalaysia-configuration',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byModule-fundMalaysia/cm0_fundMalaysia_mainSetting.vue'
                        ),
                },
                {
                    name: 'distributor-fee',
                    path: 'distributor-fee',
                    component: () =>
                        import ('../pages/admin-configuration/fee-management/distributor-fee.vue'),
                },
                {
                    name: 'waiver-fee',
                    path: 'waiver-fee',
                    component: () =>
                        import ('../pages/admin-configuration/fee-management/waiver-fee.vue'),
                },
                {
                    name: 'consultant-fee',
                    path: 'consultant-fee',
                    component: () =>
                        import ('../pages/admin-configuration/fee-management/consultant-fee.vue'),
                },
                {
                    name: 'maintenance-list',
                    path: 'maintenance-list',
                    component: () =>
                        import (
                            '../pages/admin-configuration/page-maintenance-management/cm0_maintenanceList.vue'
                        ),
                },
                {
                    name: 'maintenance-newPage',
                    path: 'maintenance-newPage',
                    component: () =>
                        import ('../pages/admin-configuration/page-maintenance-management/cm0_newPage.vue'),
                },
                {
                    name: 'maintenance-edit',
                    path: 'maintenance-edit',
                    component: () =>
                        import ('../pages/admin-configuration/page-maintenance-management/cm0_editPage.vue'),
                },
                {
                    name: 'screen-access',
                    path: 'screen-access',
                    component: () =>
                        import ('../pages/admin-configuration/screen-management/cm0_screenAccess.vue'),
                },
                {
                    name: 'screen-module',
                    path: 'screen-module',
                    component: () =>
                        import ('../pages/admin-configuration/screen-management/cm0_screenModule.vue'),
                    props: true,
                },
                {
                    name: 'd-mainlist',
                    path: 'd-mainlist',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/distributor/mainlist.vue'),
                    props: true,
                },
                {
                    name: 'd-screen-module',
                    path: 'd-screen-module',
                    component: () =>
                        import (
                            '../pages/admin-configuration/other-access/distributor/cm0_distributor_screenManagement.vue'
                        ),
                    props: true,
                },
                {
                    name: 'd-screen-access',
                    path: 'd-screen-access',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/distributor/cm0_screenAccess.vue'),
                    props: true,
                },
                {
                    name: 'c-mainlist',
                    path: 'c-mainlist',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/consultant/mainlist.vue'),
                    props: true,
                },
                {
                    name: 'c-screen-module',
                    path: 'c-screen-module',
                    component: () =>
                        import (
                            '../pages/admin-configuration/other-access/consultant/cm0_consultant_screenManagement.vue'
                        ),
                    props: true,
                },
                {
                    name: 'c-screen-access',
                    path: 'c-screen-access',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/consultant/screenAccess.vue'),
                    props: true,
                },
                {
                    name: 'third-mainlist',
                    path: 'third-mainlist',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/third-party/mainlist.vue'),
                    props: true,
                },
                {
                    name: 'third-screen-module',
                    path: 'third-screen-module',
                    component: () =>
                        import (
                            '../pages/admin-configuration/other-access/third-party/cm0_3rdParty_screenManagement.vue'
                        ),
                    props: true,
                },
                {
                    name: 'third-screen-access',
                    path: 'third-screen-access',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/third-party/screenAccess.vue'),
                    props: true,
                },
                {
                    name: 'tp-mainlist',
                    path: 'tp-mainlist',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/training-provider/mainlist.vue'),
                    props: true,
                },
                {
                    name: 'tp-screen-module',
                    path: 'tp-screen-module',
                    component: () =>
                        import (
                            '../pages/admin-configuration/other-access/training-provider/cm0_trainingProvider_screenManagement.vue'
                        ),
                    props: true,
                },
                {
                    name: 'tp-screen-access',
                    path: 'tp-screen-access',
                    component: () =>
                        import ('../pages/admin-configuration/other-access/training-provider/screenAccess.vue'),
                    props: true,
                },
                {
                    name: 'demo-pdfviewer',
                    path: 'demo-pdfviewer',
                    component: () =>
                        import ('../pages/demo/pdf-viewer'),
                },
                {
                    name: 'demo-upload',
                    path: 'demo-upload',
                    component: () =>
                        import ('../pages/demo/upload-file'),
                },
                {
                    name: 'demo-excelexport',
                    path: 'demo-excelexport',
                    component: () =>
                        import ('../pages/demo/export-excel/index'),
                },
                {
                    name: 'demo-excel',
                    path: 'demo-excel',
                    component: () =>
                        import ('../components/excel-upload/index'),
                },
                {
                    name: 'demo-excelviewer',
                    path: 'demo-excelviewer',
                    component: () =>
                        import ('../pages/demo/excel-viewer'),
                },
                {
                    name: 'demo-koolreport',
                    path: 'demo-koolreport',
                    component: () =>
                        import ('../pages/demo/koolreport'),
                },
                {
                    name: 'consultant-profile',
                    path: 'consultant-profile',
                    component: () =>
                        import ('../pages/consultant_profile.vue'),
                },
                {
                    name: 'registered-case',
                    path: 'registered-case',
                    component: () =>
                        import ('../pages/registered_case.vue'),
                },
                // fimm-consultant alert
                {
                    name: 'consultantAlert-overviewList',
                    path: 'consultantAlert-overviewList',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_overviewList.vue'),
                },
                {
                    name: 'consultantAlert-recordList',
                    path: 'consultantAlert-recordList',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_recordList.vue'),
                },
                {
                    name: 'consultantAlert-viewRecordList',
                    path: 'consultantAlert-viewRecordList',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_viewRecordList.vue'),
                },
                {
                    name: 'consultantAlert-consultantCaRecord',
                    path: 'consultantAlert-consultantCaRecord',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_consultantCaRecord.vue'),
                },
                {
                    name: 'consultantAlert-viewConsultantCaRecord',
                    path: 'consultantAlert-viewConsultantCaRecord',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consltantAlert_viewConsultantCaRecord.vue'),
                },
                {
                    name: 'consultantAlert-searchConsultant',
                    path: 'consultantAlert-searchConsultant',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_searchConsultant.vue'),
                },
                {
                    name: 'consultantAlert-registerNewCase',
                    path: 'consultantAlert-registerNewCase',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_registerNewCase.vue'),
                },
                {
                    name: 'consultantAlert-registerExistingRecord',
                    path: 'consultantAlert-registerExistingRecord',
                    component: () =>
                        import (
                            '../pages/consultant-alert/fimm/cm03_consultantAlert_registerExistingRecord.vue'
                        ),
                },
                {
                    name: 'consultantAlert-viewCaRecord',
                    path: 'consultantAlert-viewCaRecord',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_viewCaRecord.vue'),
                },
                {
                    name: 'consultantAlert-editCaRecord',
                    path: 'consultantAlert-editCaRecord',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_editCaRecord.vue'),
                },
                {
                    name: 'consultantAlert-hodLraRecordList',
                    path: 'consultantAlert-hodLraRecordList',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_hodLraRecordList.vue'),
                },
                {
                    name: 'consultantAlert-hodLraCaseVerification',
                    path: 'consultantAlert-hodLraCaseVerification',
                    component: () =>
                        import (
                            '../pages/consultant-alert/fimm/cm03_consultantAlert_hodLraCaseVerification.vue'
                        ),
                },
                {
                    name: 'consultantAlert-gmLraRecordList',
                    path: 'consultantAlert-gmLraRecordList',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_gmLraRecordList.vue'),
                },
                {
                    name: 'consultantAlert-gmLraCaseApproval',
                    path: 'consultantAlert-gmLraCaseApproval',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_gmLraCaseApproval.vue'),
                },
                {
                    name: 'consultantAlert-generateLetter',
                    path: 'consultantAlert-generateLetter',
                    component: () =>
                        import ('../pages/consultant-alert/fimm/cm03_consultantAlert_generateLetter.vue'),
                },
                {
                    name: 'f-moduleSelectionList',
                    path: 'f-moduleSelectionList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/cm04_f_moduleSelectionList.vue'),
                },

                // REVOCATION

                {
                    name: 'f-moduleRevocationList',
                    path: 'f-moduleRevocationList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/revocation/cm04_f_moduleRevocationList.vue'),
                },
                {
                    name: 'f-moduleRevocationListProgram',
                    path: 'f-moduleRevocationListProgram',
                    component: () =>
                        import ('../pages/continuing-development/fimm/revocation/cm04_f_moduleRevocationListProgram.vue'),
                },
                {
                    name: 'f-moduleRevocationListProgramDetails',
                    path: 'f-moduleRevocationListProgramDetails',
                    component: () =>
                        import ('../pages/continuing-development/fimm/revocation/cm04_f_moduleRevocationDetails.vue'),
                },

                // APEAL

                {
                    name: 'appeal-selectionList',
                    path: 'appeal-selectionList',
                    component: () =>
                        import ('../pages/continuing-development/cm04_appeal_selectionList.vue'),
                },
                {
                    name: 'd-appealPostVettingList',
                    path: 'd-appealPostVettingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_appealPostVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealPostVettingProgramList',
                    path: 'd-appealPostVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_appealPostVetting_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealPostVettingViewDetails',
                    path: 'd-appealPostVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_appealPostVetting_d_viewDetails.vue'
                        ),
                },

                {
                    name: 'd-appealPreVettingList',
                    path: 'd-appealPreVettingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealPreVettingViewDetails',
                    path: 'd-appealPreVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_d_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-appealPreVettingList',
                    path: 'f-appealPreVettingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_fimmList.vue'
                        ),
                },
                {
                    name: 'f-appealPreVettingViewDetails',
                    path: 'f-appealPreVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_f_viewDetails.vue'
                        ),
                },
                {
                    name: 't-appealPreVettingViewDetails',
                    path: 't-appealPreVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_t_viewDetails.vue'
                        ),
                },
                {
                    name: 't-appealPreVettingList',
                    path: 't-appealPreVettingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_trainingList.vue'
                        ),
                },
                {
                    name: 'd-appealPreVettingProgramList',
                    path: 'd-appealPreVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_appealPreVetting_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealWritingList',
                    path: 'd-appealWritingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_appealWriting_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealWritingListProgramList',
                    path: 'd-appealWritingListProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_appealWriting_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealWritingViewDetails',
                    path: 'd-appealWritingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_appealWriting_d_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-appealReadingList',
                    path: 'd-appealReadingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_appealReading_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealReadingListProgramList',
                    path: 'd-appealReadingListProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_appealReading_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealReadingViewDetails',
                    path: 'd-appealReadingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_appealReading_d_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-appealReadingList',
                    path: 'f-appealReadingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_appealReading_fimmList.vue'
                        ),
                },
                {
                    name: 'f-appealReadingViewDetails',
                    path: 'f-appealReadingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_appealReading_f_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-appealTeachingList',
                    path: 'd-appealTeachingList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_appealTeaching_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealTeachingListProgramList',
                    path: 'd-appealTeachingListProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_appealTeaching_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealTeachingViewDetails',
                    path: 'd-appealTeachingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_appealTeaching_d_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-appealAcademicList',
                    path: 'd-appealAcademicList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_appealAcademic_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealAcademicListProgramList',
                    path: 'd-appealAcademicListProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_appealAcademic_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealAcademicViewDetails',
                    path: 'd-appealAcademicViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_appealAcademic_d_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-appealFinancialList',
                    path: 'd-appealFinancialList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/cm04_appealFinancial_distributorList.vue'
                        ),
                },

                {
                    name: 'd-appealFinancialListProgramList',
                    path: 'd-appealFinancialListProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/cm04_appealFinancial_program_distributorList.vue'
                        ),
                },
                {
                    name: 'd-appealFinancialViewDetails',
                    path: 'd-appealFinancialViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/cm04_appealFinancial_d_viewDetails.vue'
                        ),
                },

                // MODULE 04
                // FIMM CPD
                {
                    name: 'f-postVettingProgramList',
                    path: 'f-postVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/postVetting/cm04_fPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'f-preVettingProgramList',
                    path: 'f-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'f-writingModuleList',
                    path: 'f-writingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_fWriting_list.vue'
                        ),
                },
                {
                    name: 'f-writingViewDetails',
                    path: 'f-writingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_dWriting_viewDetails.vue'
                        ),
                },
                {
                    name: 'approvalWritingDistributorList',
                    path: 'approvalWritingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_apprWriting_distributorList.vue'
                        ),
                },
                {
                    name: 'f-writingModuleList',
                    path: 'f-writingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_fWriting_list.vue'
                        ),
                },
                {
                    name: 'approvalWritingViewDetails',
                    path: 'approvalWritingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_apprWriting_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-approvalViewDetailParticipant',
                    path: 'f-approvalViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_apprWriting_viewDetailsParticipant.vue'
                        ),
                },
                {
                    name: 'approvalWritingModuleList',
                    path: 'approvalWritingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/writing/cm04_apprWriting_moduleList.vue'
                        ),
                },
                {
                    name: 'f-approvalWritingViewDetailParticipant',
                    path: 'f-approvalWritingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                // FIMM CPD -nurul
                {
                    name: 'approval-selectionList',
                    path: 'approval-selectionList',
                    component: () =>
                        import ('../pages/continuing-development/cm04_approval_selectionList.vue'),
                },

                {
                    name: 'f-postVettingProgramList',
                    path: 'f-postVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/postVetting/cm04_fPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'f-preVettingProgramList',
                    path: 'f-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_programList.vue'
                        ),
                },


                // Fimm - Post Vetting Approval
                {
                    name: 'approvalPostVettingDistributorList',
                    path: 'approvalPostVettingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_apprPostVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'approvalPostVettingProgramList',
                    path: 'approvalPostVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_apprPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'approvalPostVettingViewDetails',
                    path: 'approvalPostVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_apprPostVetting_viewDetails.vue'
                        ),
                },
                {
                    name: 'pdsPostVettingDistributorList',
                    path: 'pdsPostVettingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_pdsPostVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'pdsPostVettingProgramList',
                    path: 'pdsPostVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_pdsPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'pdsPostVettingViewDetails',
                    path: 'pdsPostVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_pdsPostVetting_viewDetails.vue'
                        ),
                },
                // ids
                {
                    name: 'idsPostVettingDistributorList',
                    path: 'idsPostVettingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_idsPostVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'idsPostVettingProgramList',
                    path: 'idsPostVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_idsPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'idsPostVettingViewDetails',
                    path: 'idsPostVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_idsPostVetting_viewDetails.vue'
                        ),
                },

                // end postvetting approval*
               
                // Fimm - Post Vetting Approval
                {
                    name: 'isdPreVettingDistributorList',
                    path: 'isdPreVettingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_isdPreVetting_distributorList.vue'
                        ),
                },
                {
                    name: 'isdPreVettingProgramList',
                    path: 'isdPreVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_isdPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'isdPreVettingViewDetails',
                    path: 'isdPreVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_isdPreVetting_viewDetails.vue'
                        ),
                },
                // ids
                {
                    name: 'approvalPreVettingDistributorList',
                    path: 'approvalPreVettingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_apprPreVetting_distributorList.vue'
                        ),
                },
                
                 //FIMM - waiver approval Nurul
                 {
                    name: 'waiver-submission-list',
                    path: 'waiver-submission-list',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/waiver/cm04_apprWaiver_submissionList.vue'
                        ),
                },
                {
                    name: 'review-waiver-submission',
                    path: 'review-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/waiver/cm04_review_waiverSubmissionForm.vue'
                        ),
                },
                {
                    name: 'viewDetails-waiver-submission',
                    path: 'viewDetails-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/waiver/cm04_apprWaiver_viewDetails.vue'
                        ),
                },
               

                // end waiver approval nurul

               

               

                /**
                 * 5 modules (fimm)
                 * start
                 */
                // reading
                {
                    name: 'approvalReadingDistributorList',
                    path: 'approvalReadingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/reading/cm04_apprReading_distributorList.vue'
                        ),
                },
                {
                    name: 'approvalReadingModuleList',
                    path: 'approvalReadingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/reading/cm04_apprReading_moduleList.vue'
                        ),
                },
                {
                    name: 'f-readingModuleList',
                    path: 'f-readingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/reading/cm04_fReading_list.vue'
                        ),
                },
                {
                    name: 'approvalReadingViewDetails',
                    path: 'approvalReadingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/reading/cm04_apprReading_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-readingViewDetails',
                    path: 'd-readingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/reading/cm04_dReading_viewDetails.vue'
                        ),
                },
                {
                    name: 'd-apprReadingViewDetailParticipant',
                    path: 'd-apprReadingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                //teaching
                {
                    name: 'approvalTeachingDistributorList',
                    path: 'approvalTeachingDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/teaching/cm04_apprTeaching_distributorList.vue'
                        ),
                },
                {
                    name: 'approvalTeachingModuleList',
                    path: 'approvalTeachingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/teaching/cm04_apprTeaching_moduleList.vue'
                        ),
                },
                {
                    name: 'f-teachingModuleList',
                    path: 'f-teachingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/teaching/cm04_fTeaching_list.vue'
                        ),
                },
                {
                    name: 'approvalTeachingViewDetails',
                    path: 'approvalTeachingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/teaching/cm04_apprTeaching_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-teachingViewDetails',
                    path: 'f-teachingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/teaching/cm04_fTeaching_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-apprTeachingViewDetailParticipant',
                    path: 'f-apprTeachingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                // qualification
                {
                    name: 'approvalAcademicDistributorList',
                    path: 'approvalAcademicDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/academic/cm04_apprAcademic_distributorList.vue'
                        ),
                },
                {
                    name: 'approvalAcademicModuleList',
                    path: 'approvalAcademicModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/academic/cm04_apprAcademic_moduleList.vue'
                        ),
                },
                {
                    name: 'f-academicModuleList',
                    path: 'f-academicModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/academic/cm04_fAcademic_list.vue'
                        ),
                },
                {
                    name: 'approvalAcademicViewDetails',
                    path: 'approvalAcademicViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/academic/cm04_apprAcademic_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-academicViewDetails',
                    path: 'f-academicViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/academic/cm04_fAcademic_viewDetails.vue'
                        ),
                },
                {
                    name: 'f-apprAcademicViewDetailParticipant',
                    path: 'f-apprAcademicViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                // financial planner
                {
                    name: 'approvalFPDistributorList',
                    path: 'approvalFPDistributorList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/cm04_apprFP_distributorList.vue'
                        ),
                },
                {
                    name: 'f-fpamModuleList',
                    path: 'f-fpamModuleList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/fpam/cm04_fFpam_list.vue'),
                },

                {
                    name: 'f-fpamCertifiedModuleList',
                    path: 'f-fpamCertifiedModuleList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/fpam/certified-financial/cm04_fCertified_module_list.vue'),
                },
                {
                    name: 'f-fpamCertifiedModuleNewSubmission',
                    path: 'f-fpamCertifiedModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/certified-financial/cm04_fCertified_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'f-fpamIslamicModuleList',
                    path: 'f-fpamIslamicModuleList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/fpam/islamic-financial/cm04_fIslamic_module_list.vue'),
                },
                {
                    name: 'f-fpamIslamicModuleNewSubmission',
                    path: 'f-fpamIslamicModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/islamic-financial/cm04_fIslamic_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'f-fpamRegisteredModuleList',
                    path: 'f-fpamRegisteredModuleList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/fpam/registered-financial/cm04_fRegistered_module_list.vue'),
                },
                {
                    name: 'f-fpamRegisteredModuleNewSubmission',
                    path: 'f-fpamRegisteredModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/registered-financial/cm04_fRegistered_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'f-fpamShariahModuleList',
                    path: 'f-fpamShariahModuleList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/fpam/shariah-registered/cm04_fShariah_module_list.vue'),
                },
                {
                    name: 'f-fpamShariahModuleNewSubmission',
                    path: 'f-fpamShariahModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/shariah-registered/cm04_fShariah_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'approvalFPViewDetails',
                    path: 'approvalFPViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/cm04_apprFpViewDetails.vue'
                        ),
                },
                {
                    name: 'f-FPViewDetails',
                    path: 'f-FPViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/modules/fpam/cm04_fpViewDetails.vue'
                        ),
                },
                {
                    name: 'f-apprFpViewDetailParticipant',
                    path: 'f-apprFpViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },
                /**
                 * 5 modules (fimm)
                 * end
                 */

                {
                    name: 'approvalPreVettingProgramList',
                    path: 'approvalPreVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_apprPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'approvalPreVettingViewDetails',
                    path: 'approvalPreVettingViewDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_apprPreVetting_viewDetails.vue'
                        ),
                },

                // end prevetting approval

                // Fimm Training - PreVetting
                {
                    name: 'f-postVettingNewSubmission',
                    path: 'f-postVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/postVetting/cm04_fPostVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'f-preVettingProgramList',
                    path: 'f-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'f-preVettingNewSubmission',
                    path: 'f-preVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'f-preVettingRepeatSubmission',
                    path: 'f-preVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 'f-preVettingViewProgramList',
                    path: 'f-preVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/fimm/preVetting/cm04_fPreVetting_viewProgramList.vue'
                        ),
                },
                // Fimm - Module
                {
                    name: 'f-modulesRecordList',
                    path: 'f-modulesRecordList',
                    component: () =>
                        import ('../pages/continuing-development/fimm/modules/cm04_fModules_recordList.vue'),
                },

                {
                    name: 'ui',
                    path: 'ui',
                    component: EmptyParentComponent,
                    children: [{
                        name: 'colors',
                        path: 'colors',
                        component: () =>
                            import ('../components/ui/colors/Colors'),
                    }, ],
                },

                {
                    name: 'system-setting',
                    path: 'system-setting',
                    component: () =>
                        import ('../pages/admin-configuration/cm0_systemSetting.vue'),
                },
                {
                    name: 'group-management',
                    path: 'group-management',
                    component: () =>
                        import ('../pages/admin-configuration/group-management/cm0_groupManagement.vue'),
                },
                {
                    name: 'distributor-mainList',
                    path: 'distributor-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/distributorApproval_mainList.vue'),
                },
                {
                    name: 'approval-mainList',
                    path: 'approval-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/cm0_approval_mainList.vue'),
                },
                // fimm approval level new distributor
                {
                    name: 'newDistributor-approval-level',
                    path: 'newDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor_approvalLevel.vue'
                        ),
                },
                {
                    name: 'newDistributor-first-rdReview',
                    path: 'newDistributor-first-rdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/firstRdReview.vue'
                        ),
                },
                {
                    name: 'newDistributor-first-HODrdReview',
                    path: 'newDistributor-first-HODrdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/firstHodRdReview.vue'
                        ),
                },
                {
                    name: 'newDistributor-LRAReview',
                    path: 'newDistributor-LRAReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/LRAreview.vue'
                        ),
                },
                {
                    name: 'newDistributor-secondLevel-RdReview',
                    path: 'newDistributor-secondLevel-RdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/secondLevel-rdReview.vue'
                        ),
                },
                {
                    name: 'newDistributor-secondLevel-Hod-RdReview',
                    path: 'newDistributor-secondLevel-Hod-RdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/secondLevel-Hod-rdReview.vue'
                        ),
                },
                {
                    name: 'newDistributor-SupervisionReview',
                    path: 'newDistributor-SupervisionReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/supervision-review.vue'
                        ),
                },
                {
                    name: 'newDistributor-GMReview',
                    path: 'newDistributor-GMReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/Gm-review.vue'
                        ),
                },
                {
                    name: 'newDistributor-CeoReview',
                    path: 'newDistributor-CeoReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/Ceo-review.vue'
                        ),
                },
                {
                    name: 'newDistributor-BodReview',
                    path: 'newDistributor-BodReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/newDistributor-approval-level/BOD-review.vue'
                        ),
                },

                // fimm Approval level Update distirbutor

                {
                    name: 'updateDistributor-approval-level',
                    path: 'updateDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/updateDistributor-approval-level.vue'
                        ),
                },
                {
                    name: 'updateDistributor-rdReview',
                    path: 'updateDistributor-rdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/updateDistributor/rdReview.vue'
                        ),
                },
                // fimm approval level suspension
                {
                    name: 'suspensionRevocationDistributor-approval-level',
                    path: 'suspensionRevocationDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocationDistributor-approval-level.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-first-HodRdReview',
                    path: 'suspensionRevocationDistributor-first-HodRdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/first-HodRdReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-first-GMReview',
                    path: 'suspensionRevocationDistributor-first-GMReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/first-GmReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-first-CeoReview',
                    path: 'suspensionRevocationDistributor-first-CeoReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/first-CeoReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-RdReview',
                    path: 'suspensionRevocationDistributor-RdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/rdReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-second-CeoReview',
                    path: 'suspensionRevocationDistributor-second-CeoReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/second-CeoReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-second-HodRdReview',
                    path: 'suspensionRevocationDistributor-second-HodRdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/second-HodRdReview.vue'
                        ),
                },
                {
                    name: 'suspensionRevocationDistributor-second-GMReview',
                    path: 'suspensionRevocationDistributor-second-GMReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/suspensionRevocation/second-GmReview.vue'
                        ),
                },
                // FIMM Extension Time Approval Level
                {
                    name: 'ExtensionDistributor-RdReview',
                    path: 'ExtensionDistributor-RdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/extension-time/rdReview.vue'
                        ),
                },
                {
                    name: 'ExtensionDistributor-HodRdReview',
                    path: 'ExtensionDistributor-HodRdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/extension-time/HodRdReview.vue'
                        ),
                },
                {
                    name: 'ExtensionDistributor-GmReview',
                    path: 'ExtensionDistributor-GmReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/extension-time/GmReview.vue'
                        ),
                },
                {
                    name: 'ExtensionDistributor-CeoReview',
                    path: 'ExtensionDistributor-CeoReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/extension-time/ceoReview.vue'
                        ),
                },
                // fimm Approval Level Distributor
               
                // Distributor Extension Time Approval Level
                {
                    name: 'ExtensionDistributor-ManagerReview',
                    path: 'ExtensionDistributor-ManagerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/extension-time/managerDistributorReview.vue'
                        ),
                },
                {
                    name: 'extensionManager-mainList',
                    path: 'extensionManager-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-distributor/manager-mainList.vue'),
                },
                // FImm approval Level Divestment

                {
                    name: 'divestmentDistributor-approval-level',
                    path: 'divestmentDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/divestmentDistributor-approval-level.vue'
                        ),
                },
                {
                    name: 'divestmentDistributor-approval-level',
                    path: 'divestmentDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/divestmentDistributor-approval-level.vue'
                        ),
                },
                {
                    name: 'divestmentDistributor-rdReview',
                    path: 'divestmentDistributor-rdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-distributor/divestment/rdReview.vue'),
                },
                {
                    name: 'divestmentDistributor-HodrdReview',
                    path: 'divestmentDistributor-HodrdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-distributor/divestment/HodRdReview.vue'
                        ),
                },
                // fimm Approval Level New Consultant
                {
                    name: 'newConsultant-approval-level',
                    path: 'newConsultant-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/newConsultant_approvalLevel.vue'
                        ),
                },
                // Fimm Consultant Termination
                {
                    name: 'terminationConsultant-approval-level',
                    path: 'terminationConsultant-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/terminationConsultant_approvalLevel.vue'
                        ),
                },
                {
                    name: 'terminationConsultant-HodRdReview',
                    path: 'terminationConsultant-HodRdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/termination/HodRdReview.vue'
                        ),
                },
                // fimm Appproval Level Bankruptcy check
                {
                    name: 'banckruptcyCheckConsultant-approval-level',
                    path: 'banckruptcyCheckConsultant-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/bankruptcyCheck_approvalLevel.vue'
                        ),
                },
                {
                    name: 'banckruptcyCheckConsultant-RdReview',
                    path: 'banckruptcyCheckConsultant-RdReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/bankruptcyCheck/RdReview.vue'
                        ),
                },

                // Fimm Approval level Appeal COnsultant
                {
                    name: 'appealConsultant-approval-level',
                    path: 'appealConsultant-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/appealConsultant_approvalLevel.vue'
                        ),
                },
                {
                    name: 'appealConsultant-RdReview',
                    path: 'appealConsultant-RdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Consultant/appeal/RdReview.vue'),
                },

                // Distributor Approval Level
                {
                    name: 'NewConsultant-mainList',
                    path: 'NewConsultant-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/newConsultant/mainList.vue'
                        ),
                },
                {
                    name: 'NewConsultant-managerReview',
                    path: 'NewConsultant-managerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/newConsultant/managerReview.vue'
                        ),
                },
                {
                    name: 'terminationConsultant-mainList',
                    path: 'terminationConsultant-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/termination/mainList.vue'
                        ),
                },
                {
                    name: 'terminationConsultant-managerReview',
                    path: 'terminationConsultant-managerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/termination/managerReview.vue'
                        ),
                },
                {
                    name: 'appealConsultant-mainList',
                    path: 'appealConsultant-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/appeal/mainList.vue'
                        ),
                },
                {
                    name: 'appealConsultant-managerReview',
                    path: 'appealConsultant-managerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/appeal/managerReview.vue'
                        ),
                },
                {
                    name: 'bankruptcyConsultant-mainList',
                    path: 'bankruptcyConsultant-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/bankruptcyCheck/mainList.vue'
                        ),
                },
                {
                    name: 'bankruptcyConsultant-managerReview',
                    path: 'bankruptcyConsultant-managerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Consultant/distributor/bankruptcyCheck/managerReview.vue'
                        ),
                },
                // -----
                // Fimm Approval Level CAS
                {
                    name: 'newCase-approval-level',
                    path: 'newCase-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase-approval-level.vue'),
                },
                {
                    name: 'newCase-Gm-mainList',
                    path: 'newCase-Gm-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/approval-mainList.vue'),
                },
                {
                    name: 'newCase-GmReview',
                    path: 'newCase-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/GmReview.vue'),
                },
                {
                    name: 'newCase-legalHod-mainList',
                    path: 'newCase-legalHod-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/verification-mainList.vue'),
                },
                {
                    name: 'newCase-legalHodReview',
                    path: 'newCase-legalHodReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/legalHodreview.vue'),
                },
                {
                    name: 'newCase-legalClerk-mainList',
                    path: 'newCase-legalCler-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/entry-mainList.vue'),
                },
                {
                    name: 'newCase-legalClerkReview',
                    path: 'newCase-legalClerkReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CAS/newCase/legalClerk.vue'),
                },

                // Fimm Approval Level Fund Creation FMS
                {
                    name: 'fundCreation-approval-level',
                    path: 'fundCreation-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fundCreation-approval-level.vue'),
                },
                {
                    name: 'fundCreation-IdReview',
                    path: 'fundCreation-IdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fimm/fundCreation/IDreview.vue'),
                },

                // FImm Approval Level FUnd Lodgement FMS

                {
                    name: 'fundLodgement-approval-level',
                    path: 'fundLodgement-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fundLodgement-approval-level.vue'),
                },
                {
                    name: 'fundLodgement-RdReview',
                    path: 'fundLodgement-RdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fimm/fundLodgement/rdReview.vue'),
                },

                // FIMM Approval Level Fund Manangement FMS
                {
                    name: 'fundManagement-approval-level',
                    path: 'fundManagement-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fundManagement-approval-level.vue'),
                },
                {
                    name: 'fundManagement-IDreview',
                    path: 'fundManagement-IDreview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fimm/fundManagement/IDreview.vue'),
                },
                // Fimm Approval Level Fund Termination FMS
                {
                    name: 'fundTermination-approval-level',
                    path: 'fundTermination-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/fundTermination-approval-level.vue'
                        ),
                },
                {
                    name: 'fundTermination-IDreview',
                    path: 'fundTermination-IDreview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fimm/fundTermination/IDreview.vue'),
                },
                // FIMM Approval Level Nav Management
                {
                    name: 'navManagement-approval-level',
                    path: 'navManagement-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/navManagement-approval-level.vue'),
                },
                {
                    name: 'navManagement-HodIDreview',
                    path: 'navManagement-HodIDreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/fimm/navManagement/HodIDreview.vue'
                        ),
                },
                {
                    name: 'navManagement-GMreview',
                    path: 'navManagement-GMreview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-FMS/fimm/navManagement/GmReview.vue'),
                },
                // Distributor Approval for FMS
                {
                    name: 'fundCreation-mainList',
                    path: 'fundCreation-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundCreation/mainList.vue'
                        ),
                },
                {
                    name: 'fundCreation-FundManagerreview',
                    path: 'fundCreation-FundManagerreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundCreation/fundManagerReview.vue'
                        ),
                },
                {
                    name: 'fundTermination-mainList',
                    path: 'fundTermination-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundTermination/mainList.vue'
                        ),
                },
                {
                    name: 'fundTermination-FundManagerreview',
                    path: 'fundTermination-FundManagerreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundTermination/fundManager.vue'
                        ),
                },
                {
                    name: 'fundLodgement-mainList',
                    path: 'fundLodgement-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundLodgement/mainList.vue'
                        ),
                },
                {
                    name: 'fundLodgement-FundManagerreview',
                    path: 'fundLodgement-FundManagerreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundLodgement/fundManagerReview.vue'
                        ),
                },
                {
                    name: 'fundManagement-mainList',
                    path: 'fundManagement-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundManagement/mainList.vue'
                        ),
                },
                {
                    name: 'fundManagement-FundManagerreview',
                    path: 'fundManagement-FundManagerreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundManagement/fundManagerReview.vue'
                        ),
                },
                {
                    name: 'NavManagement-mainList',
                    path: 'NavManagement-mainList',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/navManagement/mainList.vue'
                        ),
                },
                {
                    name: 'NavManagement-FundManagerreview',
                    path: 'NavManagement-FundManagerreview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/navManagement/fundManagerReview.vue'
                        ),
                },
                {
                    name: 'fundDislodgement-ReviewerReview',
                    path: 'fundDislodgement-ReviewerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-FMS/distributor/fundDislodgment/reviewer.vue'
                        ),
                },
                // --------
                //CESSATION APPROVAL - NURUL
                {
                    name: 'cessation-submission-overview-list',
                    path: 'cessation-submission-overview-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_cessationOverviewList.vue')
                },
                {
                    name: 'cessation-submission-list',
                    path: 'cessation-submission-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_cessationSubmission_rdList.vue')
                },
                {
                    name: 'view-cessation-submission-form',
                    path: 'view-cessation-submission-form',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_viewCessation_submissionForm.vue')
                },
                {
                    name: 'cessation-submission-rdApproval',
                    path: 'cessation-submission-rdApproval',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_reviewCessationSubmission_rdApproval.vue')
                },
                {
                    name: 'cessation-submission-hod-list',
                    path: 'cessation-submission-hod-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_cessationSubmission_hodList.vue')
                },
                {
                    name: 'cessation-submission-hodApproval',
                    path: 'cessation-submission-hodApproval',
                    component: () =>
                        import ('../pages/distributor-management/fimm/cessation/cm1_reviewCessationSubmission_hodApproval.vue')
                },

                //END CESSATION APPROVAL - NURUL

                // divestment -Ros
                {
                    name: 'distributor-DivestmentList-rdApproval',
                    path: 'distributor-DivestmentList-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/divestment/cm1_Divestment_RDReview_List.vue'
                        ),
                },
                {
                    name: 'distributor-DivestmentDetails-rdApproval',
                    path: 'distributor-DivestmentDetails-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/divestment/cm1_Divestment_RDReview_Details.vue'
                        ),
                },
                {
                    name: 'distributor-DivestmentList-Hod-rdApproval',
                    path: 'distributor-DivestmentList-Hod-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/divestment/cm1_Divestment_HOD_RDReview_List.vue'
                        ),
                },
                {
                    name: 'distributor-DivestmentDetails-Hod-rdApproval',
                    path: 'distributor-DivestmentDetails-Hod-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/divestment/cm1_Divestment_HOD_RDReview_Details.vue'
                        ),
                },

                {
                    name: 'newConsultantPayment-approval-level',
                    path: 'newConsultantPayment-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Finance/newConsultantPayment-approval-level.vue'
                        ),
                },
                {
                    name: 'newDistributorPayment-approval-level',
                    path: 'newDistributorPayment-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Finance/newDistributorPayment-approval-level.vue'
                        ),
                },
                {
                    name: 'prepaymentRefundDistributor-approval-level',
                    path: 'prepaymentRefundDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Finance/prepaymentRefundDistributor-approval-level.vue'
                        ),
                },
                {
                    name: 'prepaymentTopupDistributor-approval-level',
                    path: 'prepaymentTopupDistributor-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Finance/prepaymentTopupDistributor-approval-level.vue'
                        ),
                },
                // FIMM Apporval LEvel Annual Fee
                {
                    name: 'anualFee-approval-level',
                    path: 'anualFee-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/anualFee-approval-level.vue'),
                },
                {
                    name: 'anualFee-RdReview',
                    path: 'anualFee-RdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/fimm/RdReview.vue'),
                },
                {
                    name: 'anualFee-HodRdReview',
                    path: 'anualFee-HodRdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/fimm/HodRdReview.vue'),
                },
                {
                    name: 'anualFee-FinanceReview',
                    path: 'anualFee-FinanceReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/fimm/financeReview.vue'),
                },
                {
                    name: 'anualFee-HodFinanceReview',
                    path: 'anualFee-HodFinanceReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/fimm/HodFinanceReview.vue'),
                },
                // Distributor Approval for Annual Fee
                {
                    name: 'DistAnualFee-mainList',
                    path: 'DistAnualFee-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/distributor/mainList.vue'),
                },
                {
                    name: 'DistAnualFee-AdminReview',
                    path: 'DistAnualFee-AdminReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-AnnualFee/distributor/adminReview.vue'),
                },
                {
                    name: 'DistAnualFee-ManagerReview',
                    path: 'DistAnualFee-ManagerReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-AnnualFee/distributor/managerReview.vue'
                        ),
                },
                {
                    name: 'DistAnualFee-ComplianceReview',
                    path: 'DistAnualFee-ComplianceReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-AnnualFee/distributor/complianceOfficerReview.vue'
                        ),
                },
                // ------------
                // FImm approval Level CPD 5 module
                {
                    name: '5ModulesSubmission-approval-level',
                    path: '5ModulesSubmission-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/5module/mainlist.vue'),
                },
                {
                    name: '5ModulesSubmission-PdsReview',
                    path: '5ModulesSubmission-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/5module/PDSreview.vue'),
                },
                {
                    name: '5ModulesSubmission-GmReview',
                    path: '5ModulesSubmission-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/5module/GmReview.vue'),
                },
                // FImm approval Level CPD new Training provider
                {
                    name: 'newTrainingProvider-approval-level',
                    path: 'newTrainingProvider-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/newTrainingProvider/mainlist.vue'),
                },
                {
                    name: 'newTrainingProvider-PdsReview',
                    path: 'newTrainingProvider-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/newTrainingProvider/PdsReview.vue'),
                },
                {
                    name: 'newTrainingProvider-GmReview',
                    path: 'newTrainingProvider-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/newTrainingProvider/GmReview.vue'),
                },
                // FImm approval Level CPD post vetting
                {
                    name: 'postVettingSubmission-approval-level',
                    path: 'postVettingSubmission-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/postVetting/mainlist.vue'),
                },
                {
                    name: 'postVettingSubmission-PdsReview',
                    path: 'postVettingSubmission-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/postVetting/PdsReview.vue'),
                },
                {
                    name: 'postVettingSubmission-GmReview',
                    path: 'postVettingSubmission-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/postVetting/GmReview.vue'),
                },
                // FImm approval Level CPD pre vetting
                {
                    name: 'preVettingSubmission-approval-level',
                    path: 'preVettingSubmission-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/preVetting/mainlist.vue'),
                },
                {
                    name: 'preVettingSubmission-PdsReview',
                    path: 'preVettingSubmission-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/preVetting/PdsReview.vue'),
                },
                // FImm approval Level CPD revocation
                {
                    name: 'revocationCPD-approval-level',
                    path: 'revocationCPD-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/revocationCpd/mainlist.vue'),
                },
                {
                    name: 'revocationCPD-PdsReview',
                    path: 'revocationCPD-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/revocationCpd/PdsReview.vue'),
                },
                {
                    name: 'revocationCPD-GmReview',
                    path: 'revocationCPD-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/revocationCpd/GmReview.vue'),
                },
                // FImm approval Level CPD waiver Submission
                {
                    name: 'waiverSubmission-approval-level',
                    path: 'waiverSubmission-approval-level',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/waiver/mainlist.vue'),
                },
                {
                    name: 'waiverSubmission-PdsReview',
                    path: 'waiverSubmission-PdsReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/waiver/PdsReview.vue'),
                },
                {
                    name: 'waiverSubmission-GmReview',
                    path: 'waiverSubmission-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-CPD/waiver/GmReview.vue'),
                },
                // FImm approval Level annoucemnet
                {
                    name: 'annoucement-approval-level',
                    path: 'annoucement-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/annoucementManagement-approval-level.vue'
                        ),
                },
                {
                    name: 'annoucement-HodFimmReview',
                    path: 'annoucement-HodFimmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodFIMMreview.vue'),
                },
                {
                    name: 'annoucement-mainList',
                    path: 'annoucement-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/announcement-mainList.vue'),
                },
                {
                    name: 'annoucement-HodRDReview',
                    path: 'annoucement-HodRDReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodRdReview.vue'),
                },
                {
                    name: 'annoucement-HodSupervisionReview',
                    path: 'annoucement-HodSupervisionReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/annoucement/HodSupervisionReview.vue'
                        ),
                },
                {
                    name: 'annoucement-HodLRAReview',
                    path: 'annoucement-HodLRAReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodLRAReview.vue'),
                },
                {
                    name: 'annoucement-HodIDReview',
                    path: 'annoucement-HodIDReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodIdReview.vue'),
                },
                {
                    name: 'annoucement-HodPDReview',
                    path: 'annoucement-HodPDReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/annoucement/HodProfessionalReview.vue'
                        ),
                },
                {
                    name: 'annoucement-HodRNAReview',
                    path: 'annoucement-HodRNAReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodRnaReview.vue'),
                },
                {
                    name: 'annoucement-HodItReview',
                    path: 'annoucement-HodItReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/annoucement/HodItReview.vue'),
                },
                {
                    name: 'annoucement-HodFinanceReview',
                    path: 'annoucement-HodFinanceReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/annoucement/HodFinanceReview.vue'
                        ),
                },
                // FImm approval Level CIrcular
                {
                    name: 'circular-approval-level',
                    path: 'circular-approval-level',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/circularManagement-approval-level.vue'
                        ),
                },
                {
                    name: 'circular-mainList',
                    path: 'circular-mainList',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular-mainlist.vue'),
                },
                {
                    name: 'circular-HodFimmReview',
                    path: 'circular-HodFimmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodFIMMreview.vue'),
                },
                {
                    name: 'circular-HodRdReview',
                    path: 'circular-HodRdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodRdReview.vue'),
                },
                {
                    name: 'circular-GmRdReview',
                    path: 'circular-GmRdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmRdReview.vue'),
                },
                {
                    name: 'circular-HodLRAReview',
                    path: 'circular-HodLRAReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodLRAReview.vue'),
                },
                {
                    name: 'circular-GmLRAReview',
                    path: 'circular-GmLRAReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmLRAReview.vue'),
                },
                {
                    name: 'circular-HodSupervisionReview',
                    path: 'circular-HodSupervisionReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/circular/HodSupervisionReview.vue'
                        ),
                },
                {
                    name: 'circular-GmSupervisionReview',
                    path: 'circular-GmSupervisionReview',
                    component: () =>
                        import (
                            '../pages/admin-configuration/byApproval-Admin/circular/GmSupervisionReview.vue'
                        ),
                },
                {
                    name: 'circular-HodIdReview',
                    path: 'circular-HodIdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodIdReview.vue'),
                },
                {
                    name: 'circular-GmIdReview',
                    path: 'circular-GmIdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmIdReview.vue'),
                },
                {
                    name: 'circular-HodPdReview',
                    path: 'circular-HodPdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodPdReview.vue'),
                },
                {
                    name: 'circular-GmPdReview',
                    path: 'circular-GmPdReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmPdReview.vue'),
                },
                {
                    name: 'circular-HodRnaReview',
                    path: 'circular-HodRnaReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodRnaReview.vue'),
                },
                {
                    name: 'circular-GmRnaReview',
                    path: 'circular-GmRnaReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmRnaReview.vue'),
                },
                {
                    name: 'circular-HodItReview',
                    path: 'circular-HodItReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodItReview.vue'),
                },
                {
                    name: 'circular-GmItReview',
                    path: 'circular-GmItReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmItReview.vue'),
                },
                {
                    name: 'circular-HodFinanceReview',
                    path: 'circular-HodFinanceReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/HodFinanceReview.vue'),
                },
                {
                    name: 'circular-GmFinanceReview',
                    path: 'circular-GmFinanceReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmFinanceReview.vue'),
                },

                {
                    name: 'circular-GmReview',
                    path: 'circular-GmReview',
                    component: () =>
                        import ('../pages/admin-configuration/byApproval-Admin/circular/GmReview.vue'),
                },
                // ----------------
                {
                    name: 'distributor-SubmissionList-rdApproval',
                    path: 'distributor-SubmissionList-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_rdApproval.vue'
                        ),
                },
                {
                    name: 'distributor-SubmissionDetails-rdApproval',
                    path: 'distributor-SubmissionDetails-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_rdApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-HODrdApproval',
                    path: 'distributor-SubmissionList-HODrdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_HODrdApproval.vue'
                        ),
                },
                {
                    name: 'distributor-SubmissionDetails-HODrdApproval',
                    path: 'distributor-SubmissionDetails-HODrdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_HODrdApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-lraApproval',
                    path: 'distributor-SubmissionList-lraApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_lraApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionDetails-lraApproval',
                    path: 'distributor-SubmissionDetails-lraApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_lraApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-supervisionApproval',
                    path: 'distributor-SubmissionList-supervisionApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_supervisionApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionDetails-supervisionApproval',
                    path: 'distributor-SubmissionDetails-supervisionApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_supervisionApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-gmApproval',
                    path: 'distributor-SubmissionList-gmApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_gmApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionDetails-gmApproval',
                    path: 'distributor-SubmissionDetails-gmApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_gmApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-ceoApproval',
                    path: 'distributor-SubmissionList-ceoApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_ceoApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionDetails-ceoApproval',
                    path: 'distributor-SubmissionDetails-ceoApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_ceoApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionList-boardApproval',
                    path: 'distributor-SubmissionList-boardApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionList_boardApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-SubmissionDetails-boardApproval',
                    path: 'distributor-SubmissionDetails-boardApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorSubmissionDetails_boardApproval.vue'
                        ),
                    props: true,
                },

                // start
                {
                    name: 'distributor-RegisterType-SubmissionList-rdApproval',
                    path: 'distributor-RegisterType-SubmissionList-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_rdApproval.vue'
                        ),
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-rdApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-rdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_rdApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-HODrdApproval',
                    path: 'distributor-RegisterType-SubmissionList-HODrdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_HODrdApproval.vue'
                        ),
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-HODrdApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-HODrdApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_HODrdApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-lraApproval',
                    path: 'distributor-RegisterType-SubmissionList-lraApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_lraApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-lraApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-lraApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_lraApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-supervisionApproval',
                    path: 'distributor-RegisterType-SubmissionList-supervisionApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_supervisionApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-supervisionApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-supervisionApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_supervisionApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-gmApproval',
                    path: 'distributor-RegisterType-SubmissionList-gmApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_gmApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-gmApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-gmApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_gmApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-ceoApproval',
                    path: 'distributor-RegisterType-SubmissionList-ceoApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_ceoApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-ceoApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-ceoApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_ceoApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-boardApproval',
                    path: 'distributor-RegisterType-SubmissionList-boardApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionList_boardApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-boardApproval',
                    path: 'distributor-RegisterType-SubmissionDetails-boardApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/type-register/cm1_distributorRegisterTypeSubmissionDetails_boardApproval.vue'
                        ),
                    props: true,
                },

                // Update Distributor
                {
                    name: 'distributor-UpdateDetails-SubmissionList-RDApproval',
                    path: 'distributor-UpdateDetails-SubmissionList-RDApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorUpdateDetailsSubmissionList_rdApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-UpdateDetails-SubmissionDetails-RDApproval',
                    path: 'distributor-UpdateDetails-SubmissionDetails-RDApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/fimm/cm1_distributorUpdateDetailsSubmissionDetails_rdApproval.vue'
                        ),
                    props: true,
                },
                // end
                // Fund Creation Start
                {
                    name: 'fundCreation-Company',
                    path: 'fundCreation-Company',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm-fund-creationCompany.vue'),
                    props: true,
                },
                {
                    name: 'fundCreation-list',
                    path: 'fundCreation-list',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm-fund-creationlist.vue'),
                },
                {
                    name: 'fundCreation-review',
                    path: 'fundCreation-review',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm-fund-creation-review.vue'),
                    props: true,
                },
                // Fund Creation End
                // Fund Lodge Start
                {
                    name: 'fundLodge-submitList',
                    path: 'fundLodge-submitList',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundLodge_submitList.vue'),
                    props: true,
                },
                {
                    name: 'fundLodge-idReview',
                    path: 'fundLodge-idReview',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundLodge_idReview.vue'),
                    props: true,
                },
                // Fund Lodge End
                // Fund DisLodge Start
                {
                    name: 'fundLodge-submitList',
                    path: 'fundLodge-submitList',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundDisLodge_submitList.vue'),
                    props: true,
                },
                {
                    name: 'fundLodge-idReview',
                    path: 'fundLodge-idReview',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundDisLodge_idReview.vue'),
                    props: true,
                },
                // Fund DisLodge End
                // Fund Termination/Suspension/Closure Start
                {
                    name: 'fundApplication-SubmitList',
                    path: 'fundApplication-SubmitList',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundApplicationList.vue'),
                    props: true,
                },
                {
                    name: 'fundApplication-Approval',
                    path: 'fundApplication-Approval',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundApplicationApproval.vue'),
                    props: true,
                },
                // Fund Termination/Suspension/Closure END
                {
                    name: 'fundManagement-default',
                    path: 'fundManagement-default',
                    component: () =>
                        import ('../pages/funds-malaysia/default.vue'),
                    props: true,
                },
                {
                    name: 'nav-list',
                    path: 'nav-list',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdParty_NAVList.vue'),
                    props: true,
                },
                // NAV Management START
                {
                    name: 'nav-approval',
                    path: 'nav-approval',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_NAVApproval.vue'),
                    props: true,
                },
                {
                    name: 'nav-hod-id-review',
                    path: 'nav-hod-id-review',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_HOD_ReviewApproval.vue'),
                    props: true,
                },
                // NAVE Management END
                // annual fee
                {
                    name: 'AnnualFee-FundSubmission-Approval',
                    path: 'AnnualFee-FundSubmission-Approval',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_AMSFFundSubmission_Approval.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-FundSubmission-list',
                    path: 'AnnualFee-FundSubmission-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_AMSFFundSubmission_list.vue'),
                    props: true,
                },
                //RD
                {
                    name: 'AnnualFee-RD-Verification-list',
                    path: 'AnnualFee-RD-Verification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_Verification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-RD-AUMVerification-list',
                    path: 'AnnualFee-RD-AUMVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_AUMVerification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-RD-AUMVerification-Details',
                    path: 'AnnualFee-RD-AUMVerification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_AUMVerification_Details.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-RD-TGSVerification-list',
                    path: 'AnnualFee-RD-TGSVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_TGSVerification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-RD-TGSVerification-Details',
                    path: 'AnnualFee-RD-TGSVerification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_TGSVerification_Details.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-RD-ConsultantVerification-list',
                    path: 'AnnualFee-RD-ConsultantVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_RD_ConsultantVerification_list.vue'),
                    props: true,
                },
                //HOD RD
                {
                    name: 'AnnualFee-HODRD-Verification-list',
                    path: 'AnnualFee-HODRD-Verification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_Verification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODRD-AUMVerification-list',
                    path: 'AnnualFee-HODRD-AUMVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_AUMVerification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODRD-AUMVerification-Details',
                    path: 'AnnualFee-HODRD-AUMVerification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_AUMVerification_Details.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODRD-TGSVerification-list',
                    path: 'AnnualFee-HODRD-TGSVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_TGSVerification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODRD-TGSVerification-Details',
                    path: 'AnnualFee-HODRD-TGSVerification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_TGSVerification_Details.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODRD-ConsultantVerification-list',
                    path: 'AnnualFee-HODRD-ConsultantVerification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODRD_ConsultantVerification_list.vue'),
                    props: true,
                },
                // FIN
                {
                    name: 'AnnualFee-FIN-Verification-list',
                    path: 'AnnualFee-FIN-Verification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_FIN_Verification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-FIN-Verification-Details',
                    path: 'AnnualFee-FIN-Verification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_FIN_Verification_Details.vue'),
                    props: true,
                },
                // HOD FIN
                {
                    name: 'AnnualFee-HODFIN-Verification-list',
                    path: 'AnnualFee-HODFIN-Verification-list',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODFIN_Verification_list.vue'),
                    props: true,
                },
                {
                    name: 'AnnualFee-HODFIN-Verification-Details',
                    path: 'AnnualFee-HODFIN-Verification-Details',
                    component: () =>
                        import ('../pages/annual-fee/fimm/cm7_HODFIN_Verification_Details.vue'),
                    props: true,
                },
                // finance-ROS
                {
                    name: 'Registration-Payment',
                    path: 'Registration-Payment',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_RegistrationPayment.vue'),
                    // props: true,
                },
                {
                    name: 'DistributorRegistration-Payment-list',
                    path: 'DistributorRegistration-Payment-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_DistributorRegistration-payment-list.vue'),
                    // props: true,
                },
                {
                    name: 'DistributorRegistration-Payment-Details',
                    path: 'DistributorRegistration-Payment-Details',
                    component: () =>
                        import (
                            '../pages/finance-management/fimm/cm6_DistributorRegistration-payment-details.vue'
                        ),
                    // props: true,
                },
                {
                    name: 'ConsultantRegistration-Payment-list',
                    path: 'ConsultantRegistration-Payment-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_ConsultantRegistration-payment-list.vue'),
                    // props: true,
                },
                {
                    name: 'ConsultantRegistration-Payment-Details',
                    path: 'ConsultantRegistration-Payment-Details',
                    component: () =>
                        import (
                            '../pages/finance-management/fimm/cm6_ConsultantRegistration-payment-details.vue'
                        ),
                    // props: true,
                },
                {
                    name: 'Transaction-Ledger',
                    path: 'Transaction-Ledger',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_TransactionLedger.vue'),
                    // props: true,
                },
                {
                    name: 'DistributorAndAMSFTransaction-list',
                    path: 'DistributorAndAMSFTransaction-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_DistributorAndAMSFTransaction-list.vue'),
                    // props: true, cm6_PrePaymentTransaction-list
                },
                {
                    name: 'PrePaymentTransaction-list',
                    path: 'PrePaymentTransaction-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrePaymentTransaction-list.vue'),
                    // props: true,
                },
                {
                    name: 'PrepaymentTopup-list',
                    path: 'PrepaymentTopup-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrepaymentTopup-list.vue'),
                    // props: true,
                },
                {
                    name: 'PrepaymentTopup-Details',
                    path: 'PrepaymentTopup-Details',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrepaymentTopup-details.vue'),
                    // props: true,
                },
                {
                    name: 'PrePayment-Refund',
                    path: 'PrePayment-Refund',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrePaymentRefund.vue'),
                    // props: true,
                },
                {
                    name: 'PrepaymentRefund-DistributorTermination-list',
                    path: 'PrepaymentRefund-DistributorTermination-list',
                    component: () =>
                        import (
                            '../pages/finance-management/fimm/cm6_PrepaymentRefund-DistributorTermination-list.vue'
                        ),
                    // props: true,
                },
                {
                    name: 'PrepaymentRefund-DistributorTermination-Details',
                    path: 'PrepaymentRefund-DistributorTermination-Details',
                    component: () =>
                        import (
                            '../pages/finance-management/fimm/cm6_PrepaymentRefund-DistributorTermination-Details.vue'
                        ),
                    // props: true,
                },
                {
                    name: 'PrepaymentRefund-ExamWaiver-list',
                    path: 'PrepaymentRefund-ExamWaiver-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrepaymentRefund-ExamWaiver-list.vue'),
                    // props: true,
                },
                {
                    name: 'PrepaymentRefund-ExamWaiver-Details',
                    path: 'PrepaymentRefund-ExamWaiver-Details',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_PrepaymentRefund-ExamWaiver-Details.vue'),
                    // props: true,
                },
                {
                    name: 'AMSFCollection-list',
                    path: 'AMSFCollection-list',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_AMSFCollection-list.vue'),
                    // props: true,
                },
                {
                    name: 'AMSFCollection-details',
                    path: 'AMSFCollection-details',
                    component: () =>
                        import ('../pages/finance-management/fimm/cm6_AMSFCollection-details.vue'),
                    // props: true,
                },
                // Fund Management START
                {
                    name: 'fund-changes-list',
                    path: 'fund-changes-list',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_fundChangesList.vue'),
                    props: true,
                },
                {
                    name: 'fund-changes-approval',
                    path: 'fund-changes-approval',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_fimm_FundChangesApproval.vue'),
                    props: true,
                },
                // Fund Management END
                // 3rd Party Company Registration START
                {
                    name: 'thirdparty-submission-list',
                    path: 'thirdparty-submission-list',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_thirdparty_ListSubmission.vue'),
                    props: true,
                },
                {
                    name: 'thirdparty-idreview',
                    path: 'thirdparty-idreview',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_thirdparty_IDReview.vue'),
                    props: true,
                },
                {
                    name: 'trp-submission-list',
                    path: 'trp-submission-list',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_TRP_ListSubmission.vue'),
                    props: true,
                },
                {
                    name: 'trp-idreview',
                    path: 'trp-idreview',
                    component: () =>
                        import ('../pages/funds-malaysia/fimm/cm5_TRP_IDReview.vue'),
                    props: true,
                },
                // 3rd Party Company Registration END
                // by module-CAS START
                {
                    name: 'letter-list',
                    path: 'letter-list',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-CAS/letter/cm0_letterList.vue'),
                },
                {
                    name: 'new-letter',
                    path: 'new-letter',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-CAS/letter/cm0_newLetter.vue'),
                },
                {
                    name: 'edit-letter',
                    path: 'edit-letter',
                    component: () =>
                        import ('../pages/admin-configuration/byModule-CAS/letter/cm0_editLetter.vue'),
                },
                // by module-CAS END

                // Masking ID setting- START
                // {
                //     name: 'distributomasking',
                //     path: 'edit-letter',
                //     component: () =>
                //         import ('../pages/admin-configuration/byModule-CAS/letter/cm0_editLetter.vue'),
                // },
                // Masking ID setting- END

                //Consulatant Module
                {
                    name: 'consultant-resignation-review-Manager',
                    path: 'consultant-resignation-review-Manager',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_resignation_review_Manager.vue'
                        ),
                },
                {
                    name: 'consultant-resignation-review-Reviewer',
                    path: 'consultant-resignation-review-Reviewer',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_resignation_review_Reviewer.vue'
                        ),
                },
                {
                    name: 'consultant-termination-Second_Level',
                    path: 'consultant-termination-Second_Level',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_termination_Second_Level.vue'
                        ),
                },
                {
                    name: 'consultant-termination-Distributor',
                    path: 'consultant-termination-Distributor',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_termination_Distributor.vue'
                        ),
                },
                {
                    name: 'consultant-termination-RD_HOD',
                    path: 'consultant-termination-RD_HOD',
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_consultant_termination_RD_HOD.vue'),
                },
                {
                    name: 'consultant-termination-RD',
                    path: 'consultant-termination-RD',
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_consultant_termination_RD.vue'),
                },
                {
                    name: 'consultant-registration-approval-RD',
                    path: 'consultant-registration-approval-RD',
                    component: () =>
                        import (
                            '../pages/consultant-management/fimm/cm02_consultant_registration_approval_RD.vue'
                        ),
                },
                {
                    name: 'consultant-registration-approval-Admin',
                    path: 'consultant-registration-approval-Admin',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_registration_approval_Admin.vue'
                        ),
                },
                {
                    name: 'consultant-registration-approval-Manager',
                    path: 'consultant-registration-approval-Manager',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_registration_approval_Manager.vue'
                        ),
                },
                {
                    name: 'distributor-list',
                    path: 'distributor-list',
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_distributor_list.vue'),
                },
                {
                    name: 'consultant-list',
                    path: 'consultant-list',
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_consultant_list.vue'),
                },
                {
                    name: 'consultant-appeal-fimm',
                    path: 'consultant-appeal-fimm/:id',
                    component: () =>
                        import (
                            '../pages/consultant-management/fimm/cm02_consultant_appeal_fimm.vue'
                        ),
                },
                // {
                //     name: 'consultant-appeal-Consultant',
                //     path: 'consultant-appeal-Consultant',
                //     component: () =>
                //         import (
                //             '../pages/consultant-management/applicant/cm02_consultant_appeal_Consultant.vue'
                //         ),
                // },
                {
                    name: 'consultant-appeal-Admin',
                    path: 'consultant-appeal-Admin',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm02_consultant_appeal_Admin.vue'),
                },
                {
                    name: 'consultant-renewal-Manager',
                    path: 'consultant-renewal-Manager',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_renewal_Manager.vue'
                        ),
                },
                {
                    name: 'consultant-renewal-Reviewer',
                    path: 'consultant-renewal-Reviewer',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_renewal_Reviewer.vue'
                        ),
                },
                {
                    name: 'consultant-renewal-Admin',
                    path: 'consultant-renewal-Admin',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm02_consultant_renewal_Admin.vue'),
                },
                {
                    name: 'consultant-bankruptcy-check-Distributor',
                    path: 'consultant-bankruptcy-check-Distributor',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_bankruptcy_check_Distributor.vue'
                        ),
                },
                {
                    name: 'consultant-bankruptcy-check-RD',
                    path: 'consultant-bankruptcy-check-RD',
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_consultant_bankruptcy_check_RD.vue'),
                },
                {
                    name: 'consultant-details',
                    path: 'consultant-details/:id',
                    props: true,
                    component: () =>
                        import ('../pages/consultant-management/fimm/cm02_consultant_details.vue'),
                },
                {
                    name: 'consultant-profile-update-Distributor',
                    path: 'consultant-profile-update-Distributor',
                    component: () =>
                        import (
                            '../pages/consultant-management/distributor/cm02_consultant_profile_update_Distributor.vue'
                        ),
                },
                {
                    name: 'consultant-registration-fimm',
                    path: 'consultant-registration-fimm/:id',
                    component: () =>
                        import (
                            '../pages/consultant-management/fimm/cm02_consultant_registration_fimm.vue'
                        ),
                },
                {
                    name: 'consultant-resignation-fimm',
                    path: 'consultant-resignation-fimm/:id',
                    component: () =>
                        import (
                            '../pages/consultant-management/fimm/cm02_consultant_resignation_fimm.vue'
                        ),
                },
                {
                    name: 'consultant-reschedule-fimm',
                    path: 'consultant-reschedule-fimm/:id',
                    component: () =>
                        import (
                            '../pages/consultant-management/fimm/cm02_consultant_reschedule_fimm.vue'
                        ),
                },
                // {
                //   name: 'consultant-re-register',
                //   path: 'consultant-re-register',
                //   component: () =>
                //     import(
                //       '../pages/consultant-management/applicant/cm02_consultant_detailsRegistration.vue'
                //     ),
                // },
                {
                    name: 'fimm-extension-request',
                    path: 'extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorExtensionRequestList.vue')
                },
                {
                    name: 'extension-request-approval-list',
                    path: 'extension-request-approval-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributorExtensionRequestApprovalList.vue')
                },
                {
                    name: 'extension-request-details',
                    path: 'extension-request-details',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributorExtensionRequestListDetails.vue')
                },
                {
                    name: 'fimm-view-extension-request',
                    path: 'view-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_view_extension_request.vue')
                },
                {
                    name: 'extension-request-rd-correction',
                    path: 'extension-request-rd-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_rd_correction.vue')
                },
                {
                    path: 'extension-request-rd-hod-correction',
                    name: 'extension-request-rd-hod-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_rd_hod_correction.vue')
                },
                {
                    name: 'extension-request-rd-approval',
                    path: 'extension-request-rd-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_rd_approval.vue')
                },
                {
                    name: 'extension-request-rd-hod-approval',
                    path: 'extension-request-rd-hod-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_rd_hod_approval.vue')
                },
                {
                    name: 'extension-request-rd-gm-approval',
                    path: 'extension-request-rd-gm-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_rd_gm_approval.vue')
                },
                {
                    name: 'extension-request-ceo-approval',
                    path: 'extension-request-ceo-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_fimm_ceo_approval.vue')
                },
                {
                    name: 'fimm-subsequent-extension-request',
                    path: 'subsequent-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorSubsequentExtensionRequestList.vue')
                },
                {
                    name: 'subsequent-extension-request-details',
                    path: 'subsequent-extension-request-details',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributorSubsequentExtensionRequestListDetails.vue')
                },
                {
                    name: 'fimm-subsequent-extension-request-approval-list',
                    path: 'subsequent-extension-request-approval-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributorSubsequentExtensionRequestApprovalList.vue')
                },
                {
                    name: 'fimm-view-subsequent-extension-request',
                    path: 'subsequent-view-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_view_subsequent_extension_request.vue')
                },
                {
                    name: 'subsequent-extension-request-rd-approval',
                    path: 'subsequent-extension-request-rd-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_rd_approval.vue')
                },
                {
                    name: 'subsequent-extension-request-rd-correction',
                    path: 'subsequent-extension-request-rd-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_rd_correction.vue')
                },
                {
                    name: 'subsequent-extension-request-rd-hod-approval',
                    path: 'subsequent-extension-request-rd-hod-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_rd_hod_approval.vue')
                },
                {
                    name: 'subsequent-extension-request-rd-hod-correction',
                    path: 'subsequent-extension-request-rd-hod-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_rd_hod_correction.vue')
                },
                {
                    name: 'subsequent-extension-request-rd-gm-approval',
                    path: 'subsequent-extension-request-rd-gm-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_gm_approval.vue')
                },
                {
                    name: 'subsequent-extension-request-ceo-approval',
                    path: 'subsequent-extension-request-ceo-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_ceo_approval.vue')
                },

                //SUSPENSION-REVOCATION - BY NURUL START
                {
                    name: 'distributor-records',
                    path: 'distributor-records',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_distributorList.vue')
                },
                {
                    name: 'new-submission',
                    path: 'new-submission',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_newSubmission.vue')
                },
                {
                    name: 'suspension-revocation-list',
                    path: 'suspension-revocation-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_distributor_suspensionRevokeList.vue')
                },
                {
                    name: 'view-suspension-revocation-list',
                    path: 'view-suspension-revocation-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_distributor_viewSuspensionRevokeList.vue')
                },
                {
                    name: 'view-submission-form',
                    path: 'view-submission-form',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_viewSubmissionForm.vue')
                },
                {
                    name: 'edit-submission-form',
                    path: 'edit-submission-form',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/cm1_editSubmissionForm.vue')
                },
                {
                    name: 'suspension-hodApproval-list',
                    path: 'suspension-hodApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_distributorSuspension_hodList.vue')
                },
                {
                    name: 'view-suspensionSubmission-hod',
                    path: 'view-suspensionSubmission-hod',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_viewSuspensionSubmission_hod.vue')
                },
                {
                    name: 'suspension-gmApproval-list',
                    path: 'suspension-gmApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_distributorSuspension_gmList.vue')
                },
                {
                    name: 'view-suspensionSubmission-gm',
                    path: 'view-suspensionSubmission-gm',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_viewSuspensionSubmission_gm.vue')
                },
                {
                    name: 'suspension-ceoApproval-list',
                    path: 'suspension-ceoApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_distributorSuspension_ceoList.vue')
                },
                {
                    name: 'view-suspensionSubmission-ceo',
                    path: 'view-suspensionSubmission-ceo',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/suspend/cm1_viewSuspensionSubmission_ceo.vue')
                },
                {
                    name: 'revocation-hodApproval-list',
                    path: 'revocation-hodApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_distributorRevocation_hodList.vue')
                },
                {
                    name: 'view-revocationSubmission-hod',
                    path: 'view-revocationSubmission-hod',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_viewRevocationSubmission_hod.vue')
                },
                {
                    name: 'revocation-gmApproval-list',
                    path: 'revocation-gmApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_distributorRevocation_gmList.vue')
                },
                {
                    name: 'view-revocationSubmission-gm',
                    path: 'view-revocationSubmission-gm',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_viewRevocationSubmission_gm.vue')
                },
                {
                    name: 'revocation-ceoApproval-list',
                    path: 'revocation-ceoApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_distributorRevocation_ceoList.vue')
                },
                {
                    name: 'view-revocationSubmission-ceo',
                    path: 'view-revocationSubmission-ceo',
                    component: () =>
                        import ('../pages/distributor-management/fimm/suspend-revoke/revoke/cm1_viewRevocationSubmission_ceo.vue')
                },

                //SUSPENSION-REVOCATION - BY NURUL END

                //APPEAL SUSPENSION-REVOCATION - BY NURUL START
                {
                    name: 'overview-appeal-suspension-revocation-list',
                    path: 'overview-appeal-suspension-revocation-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/cm1_overviewList.vue')
                },

                {
                    name: 'appeal-suspension-revocation-list',
                    path: 'appeal-suspension-revocation-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/cm1_newAppeal_distributorSubmission.vue')
                },
                {
                    name: 'view-appeal-submission',
                    path: 'view-appeal-submission',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/cm1_viewAppealSubmissionForm.vue')
                },
                {
                    name: 'review-appeal-submission',
                    path: 'review-appeal-submission',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/cm1_reviewAppealForm.vue')
                },
                {
                    name: 'appealSuspension-hodApproval-list',
                    path: 'appealSuspension-hodApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_appealSuspend_submission_hodList.vue')
                },
                {
                    name: 'view-appealSuspension-hod',
                    path: 'view-appealSuspension-hod',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_reviewAppealSuspension_hod.vue')
                },
                {
                    name: 'appealSuspension-gmApproval-list',
                    path: 'appealSuspension-gmApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_appealSuspend_submission_gmList.vue')
                },
                {
                    name: 'view-appealSuspension-gm',
                    path: 'view-appealSuspension-gm',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_reviewAppealSuspension_gm.vue')
                },
                {
                    name: 'appealSuspension-ceoApproval-list',
                    path: 'appealSuspension-ceoApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_appealSuspend_submission_ceoList.vue')
                },
                {
                    name: 'view-appealSuspension-ceo',
                    path: 'view-appealSuspension-ceo',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-suspend/cm1_reviewAppealSuspension_ceo.vue')
                },
                {
                    name: 'appealRevocation-hodApproval-list',
                    path: 'appealRevocation-hodApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_appealRevoke_submission_hodList.vue')
                },
                {
                    name: 'view-appealRevocation-hod',
                    path: 'view-appealRevocation-hod',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_reviewAppealRevocation_hod.vue')
                },
                {
                    name: 'appealRevocation-gmApproval-list',
                    path: 'appealRevocation-gmApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_appealRevoke_submission_gmList.vue')
                },
                {
                    name: 'view-appealRevocation-gm',
                    path: 'view-appealRevocation-gm',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_reviewAppealRevocation_gm.vue')
                },
                {
                    name: 'appealRevocation-ceoApproval-list',
                    path: 'appealRevocation-ceoApproval-list',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_appealRevoke_submission_ceoList.vue')
                },
                {
                    name: 'view-appealRevocation-ceo',
                    path: 'view-appealRevocation-ceo',
                    component: () =>
                        import ('../pages/distributor-management/fimm/appeal-suspend-revoke/appeal-revoke/cm1_reviewAppealRevocation_ceo.vue')
                },

                //APPEAL SUSPENSION-REVOCATION - BY NURUL END


            ],
            beforeEnter: ifAuthenticatedFiMM,
        },
        /* ---------------DISTRIBUTOR PAGES--------------------- */
        {
            name: 'Distributor',
            path: '/distributor',
            component: AppLayout,
            meta: {
                progress: {
                    func: [
                        { call: 'color', modifier: 'temp', argument: '#ffb000' },
                        { call: 'fail', modifier: 'temp', argument: '#6e0000' },
                        { call: 'location', modifier: 'temp', argument: 'top' },
                        {
                            call: 'transition',
                            modifier: 'temp',
                            argument: { speed: '1.5s', opacity: '0.6s', termination: 400 },
                        },
                    ],
                },
            },
            children: [{
                    name: 'distributor-dashboard',
                    path: 'dashboard',
                    component: () =>
                        import ('../components/dashboard/Dashboard.vue'),
                    // default: true,
                },
                // Update Distributor
                {
                    name: 'distributor-UpdateDetails-SubmissionList-secondApproval',
                    path: 'distributor-UpdateDetails-SubmissionList-secondApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorUpdateDetailsSubmissionList_2ndApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-UpdateDetails-SubmissionDetails-secondApproval',
                    path: 'distributor-UpdateDetails-SubmissionDetails-secondApproval',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorUpdateDetailsSubmissionDetails_2ndApproval.vue'
                        ),
                    props: true,
                },
                {
                    name: 'distributor-self-registration',
                    path: 'distributor-self-registration',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributor_selfRegistration.vue'
                        ),
                },
                {
                    name: 'distributor-profile',
                    path: 'distributor-profile',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributor_viewProfile.vue'
                        ),
                },
                {
                    name: 'distributor-profile-update',
                    path: 'distributor-profile-update',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributor_updateProfile.vue'
                        ),
                },
                {
                    name: 'distributor-RegisterType-SubmissionList-2ndLevel',
                    path: 'distributor-RegisterType-SubmissionList-2ndLevel',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorNewLicenseSubmissionList_2ndLevel.vue'
                        ),
                },
                {
                    name: 'distributor-RegisterType-SubmissionDetails-2ndLevel',
                    path: 'distributor-RegisterType-SubmissionDetails-2ndLevel',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorNewLicenseSubmissionDetails_2ndLevel.vue'
                        ),
                },
                {
                    name: 'distributor-register-new-license',
                    path: 'distributor-register-new-license',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributor_registerNewLicense.vue'
                        ),
                },
                {
                    name: 'distributor-details-registration',
                    path: 'distributor-details-registration',
                    props: true,
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributor_detailsRegistration.vue'
                        ),
                },

                //CESSATION SUBMISSION - NURUL
                {
                    name: 'cessation-submission',
                    path: 'cessation-submission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_cessation_applicationList.vue')
                },
                {
                    name: 'new-cessation-submission',
                    path: 'new-cessation-submission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_newCessation_submission.vue')
                },
                {
                    name: 'view-cessation-submission',
                    path: 'view-cessation-submission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_viewCessation_submission.vue')
                },
                {
                    name: 'edit-cessation-submission',
                    path: 'edit-cessation-submission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_editCessation_submission.vue')
                },
                {
                    name: 'cessation-submission-manager-approval',
                    path: 'cessation-submission-manager-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_cessation_applicationList_managerApproval.vue')
                },
                {
                    name: 'review-cessation-manager-approval',
                    path: 'review-cessation-manager-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cessation/cm1_reviewCessationSubmission_managerApproval.vue')
                },


                //END CESSATION - NURUL
                // start--acap
                {
                    name: 'distributor-member-selfRegistration',
                    path: 'distributor-member-selfRegistration',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorMember_selfRegistration.vue'
                        ),
                },
                //
                {
                    name: 'announcement',
                    path: 'announcement',
                    component: () =>
                        import ('../pages/admin-configuration/announcement/index.vue'),
                    props: { role: 'distributor' },
                },
                {
                    name: 'pending-task',
                    path: 'pending-task',
                    component: () =>
                        import ('../pages/admin-configuration/pending-task/index.vue'),
                    props: { role: 'distributor' },
                },
                {
                    name: 'notification',
                    path: 'notification',
                    component: () =>
                        import ('../pages/admin-configuration/notification/index.vue'),
                    props: { role: 'distributor' },
                },
                {
                    name: 'profile',
                    path: 'profile',
                    component: () =>
                        import ('../pages/admin-configuration/profile/index.vue'),
                    props: { role: 'distributor' },
                },
                //
                // {
                //     name: 'distributor-member-detailsRegistration',
                //     path: 'distributor-member-detailsRegistration',
                //     component: () =>
                //         import (
                //             '../pages/distributor-management/distributor/cm1_distributorMember_detailsRegistration.vue'
                //         ),
                // },
                {
                    name: 'distributor-member-verification',
                    path: 'distributor-member-verification',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorMember_verification.vue'
                        ),
                },
                {
                    name: 'distributor-member-groupSelection',
                    path: 'distributor-member-groupSelection',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorMember_groupSelection.vue'
                        ),
                },
                {
                    name: 'distributor-member-approval',
                    path: 'distributor-member-approval',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorMember_approval.vue'
                        ),
                },
                // fund creation
                {
                    name: 'fundCreation',
                    path: 'fundCreation',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_fundCreation.vue'),
                    props: true,
                },
                {
                    name: 'fundCreationlist',
                    path: 'fundCreationList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor-fundCreationList.vue'),
                    props: true,
                },
                {
                    name: 'fundCreationSecondLevelReviewlist',
                    path: 'fundCreationSecondLevelReviewList',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor-fundCreationListReview.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fundCreationSecondLevelReview',
                    path: 'fundCreationSecondLevelReview',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor_fundCreationSecondReview.vue'
                        ),
                    props: true,
                },

                {
                    name: 'funddemo',
                    path: 'funddemo',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/demoupload.vue'
                        ),
                    props: true,
                },


                // fund lodge
                {
                    name: 'fundLodgementCompany',
                    path: 'fundLodgementCompany',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundLodgeCompany.vue'),
                    props: true,
                },
                {
                    name: 'fundLodgementList',
                    path: 'fundLodgementList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundLodgeList.vue'),
                    props: true,
                },
                {
                    name: 'fundLodgementNew',
                    path: 'fundLodgementNew',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundLodgeSubmitDetails.vue'),
                    props: true,
                },
                {
                    name: 'fundDisLodgementDetails',
                    path: 'fundDisLodgementDetails',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributorFundDislodgeSubmitDetails.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fundDisLodgementSubmissionDetails',
                    path: 'fundDisLodgementSubmissionDetails',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributorFundDislodgeSubmitDetails.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fundLodgementSubmissionDetails',
                    path: 'fundLodgementSubmissionDetails',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundLodgeSubmitDetails.vue'),
                    props: true,
                },
                {
                    name: 'fundLodgementSubmissionList',
                    path: 'fundLodgementSubmissionList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundLodgeSubmitList.vue'),
                    props: true,
                },
                {
                    name: 'fundDisLodgementSubmissionList',
                    path: 'fundDisLodgementSubmissionList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorFundDislodgeSubmitList.vue'),
                    props: true,
                },
                {
                    name: 'fundlodgeSubmissionReview',
                    path: 'fundlodgeSubmissionReview',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_fundLodgeSubmitReview.vue'),
                    props: true,
                },
                {
                    name: 'fundLodgeProfile-review',
                    path: 'fundLodgeProfile-review',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor_fundLodgeProfileReview.vue'
                        ),
                    props: true,
                },
                // Fund Termination/Suspension/Closure
                {
                    name: 'fund_ApplicationList',
                    path: 'fund_ApplicationList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_FundApplicationList.vue'),
                    props: true,
                },
                {
                    name: 'fund_ApplicationDetails',
                    path: 'fund_ApplicationDetails',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor_FundApplicationDetails.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fund_ApplicationFMList',
                    path: 'fund_ApplicationFMList',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor_FundManagerFundApplicationList.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fund_ApplicationFMReview',
                    path: 'fund_ApplicationFMReview',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributorfundApplicationReview.vue'),
                    props: true,
                },
                // Funds Management
                {
                    name: 'fundManagementList',
                    path: 'fundManagementList',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_MemberFundList.vue'),
                    props: true,
                },
                {
                    name: 'fundManagementEdit',
                    path: 'fundManagementEdit',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_FundChangeDetails.vue'),
                    props: true,
                },
                {
                    name: 'fundManagementSecondLevelApprovalList',
                    path: 'fundManagementSecondLevelApprovalList',
                    component: () =>
                        import (
                            '../pages/funds-malaysia/distributor/cm5_distributor_FundSecondApprovalList.vue'
                        ),
                    props: true,
                },
                {
                    name: 'fundManagementSecondLevelApproval',
                    path: 'fundManagementSecondLevelApproval',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_FundSecondApproval.vue'),
                    props: true,
                },
                // NAV Daily Update

                {
                    name: 'navDailyUpdate',
                    path: 'navDailyUpdate',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_NAVDailyUpdate.vue'),
                    props: true,
                },

                {
                    name: 'navUpdateVerify',
                    path: 'navUpdateVerify',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_NAVUpdatetVerify.vue'),
                    props: true,
                },

                // NAV Management

                {
                    name: 'navManagement',
                    path: 'navManagement',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_NAVManagement.vue'),
                    props: true,
                },

                // CPD
                // DIstr - PostVetting
                {
                    name: 'd-moduleSelectionList',
                    path: 'd-moduleSelectionList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/cm04_d_moduleSelectionList.vue'),
                },
                {
                    name: 'd-postVettingProgramList',
                    path: 'd-postVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'd-postVettingNewSubmission',
                    path: 'd-postVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-postVettingRepeatSubmission',
                    path: 'd-postVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 'd-postVettingViewProgramDetails',
                    path: 'd-postVettingViewProgramDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_viewProgramDetails.vue'
                        ),
                },

                {
                    name: 'd-postVettingAmendProgram',
                    path: 'd-postVettingAmendProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostvetting_amendProgram.vue'
                        ),
                },
                {
                    name: 'd-postVettingEditProgram',
                    path: 'd-postVettingEditProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_editProgram.vue'
                        ),
                },
                {
                    name: 'd-postVettingViewParticipant',
                    path: 'd-postVettingViewParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostvetting_viewParticipant.vue'
                        ),
                },
                {
                    name: 'd-postVettingViewProgramList',
                    path: 'd-postVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_viewProgramList.vue'
                        ),
                },
                // dist-prevet
                {
                    name: 'd-preVettingProgramList',
                    path: 'd-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'd-preVettingNewSubmission',
                    path: 'd-preVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-preVettingRepeatSubmission',
                    path: 'd-preVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 'd-preVettingViewProgramList',
                    path: 'd-preVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_viewProgramList.vue'
                        ),
                },
                {
                    name: 'd-preVettingAmendProgram',
                    path: 'd-preVettingAmendProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPrevettig_amendProgram.vue'
                        ),
                },
                {
                    name: 'd-preVettingEditProgram',
                    path: 'd-preVettingEditProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_editProgram.vue'
                        ),
                },
                {
                    name: 'd-preVettingViewProgramDetails',
                    path: 'd-preVettingViewProgramDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_viewProgramDetails.vue'
                        ),
                },
                {
                    name: 'd-preVettingViewDetailParticipant',
                    path: 'd-preVettingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_apprPreVetting_viewParticipant.vue'
                        ),
                },
                // WAIVER START - NURUL
                {
                    name: 'waiversubmissionlist',
                    path: 'waiversubmissionlist',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/waiver/cm04_waiver_submissionList.vue'
                        ),
                },
                {
                    name: 'new-waiver-submission',
                    path: 'new-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/waiver/cm04_waiver_newSubmission.vue'
                        ),
                },
                {
                    name: 'view-waiver-submission',
                    path: 'view-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/waiver/cm04_view_waiverSubmission.vue'
                        ),
                },
                {
                    name: 'edit-waiver-submission',
                    path: 'edit-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/waiver/cm04_edit_waiverSubmission.vue'
                        ),
                },
                {
                    name: 'resubmit-waiver-submission',
                    path: 'resubmit-waiver-submission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/waiver/cm04_resubmit_waiverSubmission.vue'
                        ),
                },
                // WAIVER NURUL- END
                //cpd -dist 5 modules
                {
                    name: 'navSecondLevelApproval',
                    path: 'navSecondLevelApproval',
                    component: () =>
                        import ('../pages/funds-malaysia/distributor/cm5_distributor_NAVApproval.vue'),
                    props: true,
                },

                // end--acap
                {
                    name: 'distributor-suspension-appeal',
                    path: 'distributor-suspension-appeal',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorSuspension_appeal.vue'
                        ),
                },
                {
                    name: 'distributor-candidate-registrationList',
                    path: 'distributor-candidate-registrationList',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorBulkUpload_candidateRegisterList.vue'
                        ),
                },
                {
                    name: 'distributor-prepayment-topup',
                    path: 'distributor-prepayment-topup',
                    component: () =>
                        import ('../pages/finance-management/distributor/Cm6_PrePayment_Topup.vue'),
                },
                {
                    name: 'distributor-prepayment-topup-return',
                    path: 'distributor-prepayment-topup-return',
                    component: () =>
                        import ('../pages/finance-management/distributor/Cm6_PrePayment_Topup_return.vue'),
                },
                {
                    name: 'distributor-Transaction-List',
                    path: 'distributor-Transaction-List',
                    component: () =>
                        import (
                            '../pages/finance-management/distributor/cm6_Distributor-Transaction-Ledger-list.vue'
                        ),
                },
                {
                    name: 'distributor-AMSF-Collection',
                    path: 'distributor-AMSF-Collection',
                    component: () =>
                        import ('../pages/finance-management/distributor/cm6_Distributor_AMSFSubmission.vue'),
                },
                {
                    name: 'distributor-candidate-generateAcceptanceList',
                    path: 'distributor-candidate-generateAcceptanceList',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorBulkUpload_generateAcceptanceList.vue'
                        ),
                },
                {
                    name: 'distributor-candidate-acceptanceList_detail',
                    path: 'distributor-candidate-acceptanceList_detail',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/cm1_distributorBulkUpload_acceptanceListDetail.vue'
                        ),
                },

                //NEW candidate acceptance list - nurul
                {
                    name: 'candidateAcceptance-list',
                    path: 'candidateAcceptance-list',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/candidate-acceptance/cm1_candidateAcceptance_list.vue'
                        ),
                },
                {
                    name: 'new-candidateAcceptance',
                    path: 'new-candidateAcceptance',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/candidate-acceptance/cm1_newAcceptanceUpload.vue'
                        ),
                },
                {
                    name: 'view-candidateAcceptanceList',
                    path: 'view-candidateAcceptanceList',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/candidate-acceptance/cm1_viewAcceptanceList.vue'
                        ),
                },
                {
                    name: 'edit-candidateAcceptanceUpload',
                    path: 'edit-candidateAcceptanceUpload',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/candidate-acceptance/cm1_editAcceptanceUpload.vue'
                        ),
                },
                {
                    name: 'search-candidateRecord',
                    path: 'search-candidateRecord',
                    component: () =>
                        import (
                            '../pages/distributor-management/distributor/candidate-acceptance/cm1_searchCandidate.vue'
                        ),
                },


                // CPD
                // DIstr - PostVetting
                {
                    name: 'd-moduleSelectionList',
                    path: 'd-moduleSelectionList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/cm04_d_moduleSelectionList.vue'),
                },
                {
                    name: 'd-postVettingProgramList',
                    path: 'd-postVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_programList.vue'
                        ),
                },
                {
                    name: 'd-postVettingNewSubmission',
                    path: 'd-postVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-postVettingRepeatSubmission',
                    path: 'd-postVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 'd-postVettingViewProgramDetails',
                    path: 'd-postVettingViewProgramDetails',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_viewProgramDetails.vue'
                        ),
                },
                {
                    name: 'd-postVettingAmendProgram',
                    path: 'd-postVettingAmendProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostvetting_amendProgram.vue'
                        ),
                },
                {
                    name: 'd-postVettingEditProgram',
                    path: 'd-postVettingEditProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_editProgram.vue'
                        ),
                },
                {
                    name: 'd-postVettingViewProgramList',
                    path: 'd-postVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/postVetting/cm04_dPostVetting_viewProgramList.vue'
                        ),
                },
                // dist-prevet
                {
                    name: 'd-preVettingProgramList',
                    path: 'd-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_programList.vue'
                        ),
                },
                {
                    name: 'd-preVettingNewSubmission',
                    path: 'd-preVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-preVettingRepeatSubmission',
                    path: 'd-preVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 'd-preVettingViewProgramList',
                    path: 'd-preVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_viewProgramList.vue'
                        ),
                },
                {
                    name: 'd-preVettingAmendProgram',
                    path: 'd-preVettingAmendProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPrevettig_amendProgram.vue'
                        ),
                },
                {
                    name: 'd-preVettingEditProgram',
                    path: 'd-preVettingEditProgram',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPreVetting_editProgram.vue'
                        ),
                },
                {
                    name: 'd-preVettingViewParticipant',
                    path: 'd-preVettingViewParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/preVetting/cm04_dPrevetting_viewParticipant.vue'
                        ),
                },
              
                //cpd -dist 5 modules
                //writing
                {
                    name: 'd-writingModuleList',
                    path: 'd-writingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_dWriting_list.vue'
                        ),
                },
                {
                    name: 'd-writingNewSubmission',
                    path: 'd-writingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_dWriting_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-writingEditSubmission',
                    path: 'd-writingEditSubmission/:id',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_dWriting_editSubmission.vue'
                        ),
                },
                {
                    name: 'd-writingViewDetailParticipant',
                    path: 'd-writingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },
                {
                    name: 'd-writingAmend',
                    path: 'd-writingAmend',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/writing/cm04_dWriting_ammend.vue'
                        ),
                },


                //teaching
                {
                    name: 'd-teachingModuleList',
                    path: 'd-teachingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_dTeaching_list.vue'
                        ),
                },
                {
                    name: 'd-teachingNewSubmission',
                    path: 'd-teachingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_dTeaching_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-teachingViewDetailParticipant',
                    path: 'd-teachingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },
                {
                    name: 'd-teachingEditSubmission',
                    path: 'd-teachingEditSubmission/:id',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_dTeaching_editSubmission.vue'
                        ),
                },

                //academic
                {
                    name: 'd-academicModuleList',
                    path: 'd-academicModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_dAcademic_list.vue'
                        ),
                },
                {
                    name: 'd-academicNewSubmission',
                    path: 'd-academicNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_dAcademic_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-academicEditSubmission',
                    path: 'd-academicEditSubmission/:id',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_dAcademic_editSubmission.vue'
                        ),
                },
                {
                    name: 'd-academicViewDetailParticipant',
                    path: 'd-academicViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                //fpam
                {
                    name: 'd-fpamModuleList',
                    path: 'd-fpamModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/cm04_dFpam_list.vue'),
                },
                {
                    name: 'd-fpamCertifiedModuleList',
                    path: 'd-fpamCertifiedModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/certified-financial/cm04_dCertified_module_list.vue'),
                },
                {
                    name: 'd-fpEditSubmission',
                    path: 'd-fpEditSubmission/:id',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/cm04_dFP_editSubmission.vue'),
                },

                {
                    name: 'd-fpamCertifiedModuleNewSubmission',
                    path: 'd-fpamCertifiedModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/certified-financial/cm04_dCertified_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-fpamIslamicModuleList',
                    path: 'd-fpamIslamicModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/islamic-financial/cm04_dIslamic_module_list.vue'),
                },
                {
                    name: 'd-fpamIslamicModuleNewSubmission',
                    path: 'd-fpamIslamicModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/islamic-financial/cm04_dIslamic_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-fpamRegisteredModuleList',
                    path: 'd-fpamRegisteredModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/registered-financial/cm04_dRegistered_module_list.vue'),
                },
                {
                    name: 'd-fpamRegisteredModuleNewSubmission',
                    path: 'd-fpamRegisteredModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/registered-financial/cm04_dRegistered_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-fpamShariahModuleList',
                    path: 'd-fpamShariahModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/shariah-registered/cm04_dShariah_module_list.vue'),
                },
                {
                    name: 'd-fpamShariahModuleNewSubmission',
                    path: 'd-fpamShariahModuleNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/fpam/shariah-registered/cm04_dShariah_module_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-fpViewDetailParticipant',
                    path: 'd-fpViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                //teaching
                {
                    name: 'd-teachingModuleList',
                    path: 'd-teachingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/teaching/cm04_dTeaching_list.vue'
                        ),
                },
                //academic
                {
                    name: 'd-academicModuleList',
                    path: 'd-academicModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/academic/cm04_dAcademic_list.vue'
                        ),
                },
                //Reading
                {
                    name: 'd-readingModuleList',
                    path: 'd-readingModuleList',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_dReading_list.vue'
                        ),
                },
                {
                    name: 'd-readingNewSubmission',
                    path: 'd-readingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_dReading_newSubmission.vue'
                        ),
                },
                {
                    name: 'd-readingEditSubmission',
                    path: 'd-readingEditSubmission/:id',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/reading/cm04_dReading_editSubmission.vue'
                        ),
                },
                {
                    name: 'd-readingViewDetailParticipant',
                    path: 'd-readingViewDetailParticipant',
                    component: () =>
                        import (
                            '../pages/continuing-development/distributor/modules/components/viewParticipant.vue'
                        ),
                },

                //fpam
                {
                    name: 'd-fpamModuleList',
                    path: 'd-fpamModuleList',
                    component: () =>
                        import ('../pages/continuing-development/distributor/modules/fpam/cm04_dFpam_list.vue'),
                },

                // end CPD
                // start Consultant Management
                {
                    name: 'd-consultantReviewList',
                    path: 'd-consultantReviewList',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm2_distributor_Reviewlist.vue'),
                },
                {
                    name: 'd-consultantReviewApproval',
                    path: 'd-consultantReviewApproval',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm2_distributor_Review.vue'),
                },
                {
                    name: 'd-secondlvl-consultantReviewList',
                    path: 'd-secondlvl-consultantReviewList',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm2_secondlvl_Reviewlist.vue'),
                },
                {
                    name: 'd-secondlvl-consultantReviewApproval',
                    path: 'd-secondlvl-consultantReviewApproval',
                    component: () =>
                        import ('../pages/consultant-management/distributor/cm2_secondlvl_Review.vue'),
                },
                // End Consultant Management
                // start annual fees -ros
                {
                    name: 'distributor-Submission',
                    path: 'distributor-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_Distributor_Submission.vue'),
                },
                {
                    name: 'distributor-AUM-Submission',
                    path: 'distributor-AUM-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_Distributor_AUM_Submission.vue'),
                },
                {
                    name: 'distributor-AUM-Submission-List',
                    path: 'distributor-AUM-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_Distributor_AUM_Submission_List.vue'),
                },
                {
                    name: 'distributor-TGS-Submission',
                    path: 'distributor-TGS-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_Distributor_TGS_Submission.vue'),
                },
                {
                    name: 'distributor-TGS-Submission-List',
                    path: 'distributor-TGS-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_Distributor_TGS_Submission_List.vue'),
                },
                {
                    name: 'DistributorManager-Submission',
                    path: 'DistributorManager-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorManager_Submission.vue'),
                },
                {
                    name: 'DistributorManager-AUM-Submission',
                    path: 'DistributorManager-AUM-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorManager_AUM_Submission.vue'),
                },
                {
                    name: 'DistributorManager-AUM-Submission-List',
                    path: 'DistributorManager-AUM-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorManager_AUM_Submission_List.vue'),
                },
                {
                    name: 'DistributorManager-TGS-Submission',
                    path: 'DistributorManager-TGS-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorManager_TGS_Submission.vue'),
                },
                {
                    name: 'DistributorManager-TGS-Submission-List',
                    path: 'DistributorManager-TGS-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorManager_TGS_Submission_List.vue'),
                },
                {
                    name: 'DistributorCompliance-Submission',
                    path: 'DistributorCompliance-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorCompliance_Submission.vue'),
                },
                {
                    name: 'DistributorCompliance-AUM-Submission',
                    path: 'DistributorCompliance-AUM-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorCompliance_AUM_Submission.vue'),
                },
                {
                    name: 'DistributorCompliance-AUM-Submission-List',
                    path: 'DistributorCompliance-AUM-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorCompliance_AUM_Submission_List.vue'),
                },
                {
                    name: 'DistributorCompliance-TGS-Submission',
                    path: 'DistributorCompliance-TGS-Submission',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorCompliance_TGS_Submission.vue'),
                },
                {
                    name: 'DistributorCompliance-TGS-Submission-List',
                    path: 'DistributorCompliance-TGS-Submission-List',
                    component: () =>
                        import ('../pages/annual-fee/distributor/cm7_DistributorCompliance_TGS_Submission_List.vue'),
                },
                // end annual fee
                // start divestment -Ros
                {
                    name: 'DistributorDivestment-List',
                    path: 'DistributorDivestment-List',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestmentList.vue'),
                },
                {
                    name: 'DistributorDivestment-Submission',
                    path: 'DistributorDivestment-Submission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_Submission.vue'),
                },
                {
                    name: 'DistributorDivestment-Fund',
                    path: 'DistributorDivestment-Fund',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundSelection.vue'),
                },
                {
                    name: 'DistributorDivestment-Consultant',
                    path: 'DistributorDivestment-Consultant',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_ConsultantSelection.vue'),
                },
                {
                    name: 'DistributorDivestment-FundConsultant',
                    path: 'DistributorDivestment-FundConsultant',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundConsultantSelection.vue'),
                },
                {
                    name: 'DistributorDivestment-EditFund',
                    path: 'DistributorDivestment-EditFund',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundSelectionEdit.vue'),
                },
                {
                    name: 'DistributorDivestment-EditConsultant',
                    path: 'DistributorDivestment-EditConsultant',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_ConsultantSelectionEdit.vue'),
                },
                {
                    name: 'DistributorDivestment-EditFundConsultant',
                    path: 'DistributorDivestment-EditFundConsultant',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundConsultantSelectionEdit.vue'),
                },
                {
                    name: 'DistributorDivestment-FundSubmission',
                    path: 'DistributorDivestment-FundSubmission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundSubmission.vue'),
                },
                {
                    name: 'DistributorDivestment-ConsultantSubmission',
                    path: 'DistributorDivestment-ConsultantSubmission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_ConsultantSubmission.vue'),
                },
                {
                    name: 'DistributorDivestment-FundConsultantSubmission',
                    path: 'DistributorDivestment-FundConsultantSubmission',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundConsultantSubmission.vue'),
                },
                //testcheckbox
                {
                    name: 'Test-Checkbox',
                    path: 'Test-Checkbox',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/testcheckbox.vue'),
                },
                {
                    name: 'DistributorDivestment-FundDetails',
                    path: 'DistributorDivestment-FundDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundDetails.vue'),
                },
                {
                    name: 'DistributorDivestment-ConsultantDetails',
                    path: 'DistributorDivestment-ConsultantDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_ConsultantDetails.vue'),
                },
                {
                    name: 'DistributorDivestment-FundConsultantDetails',
                    path: 'DistributorDivestment-FundConsultantDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundConsultantDetails.vue'),
                },
                {
                    name: 'DistributorDivestment-ApprovalList',
                    path: 'DistributorDivestment-ApprovalList',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestmentList__secondApproval.vue'),
                },
                // {
                //     name: 'DistributorDivestment-ApprovalDetails',
                //     path: 'DistributorDivestment-ApprovalDetails',
                //     component: () =>
                //         import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestmentDetails_secondApproval.vue'),
                // },
                {
                    name: 'DistributorDivestment-ApprovalFundDetails',
                    path: 'DistributorDivestment-ApprovalFundDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundDetails_secondApproval.vue'),
                },
                {
                    name: 'DistributorDivestment-ApprovalConsultantDetails',
                    path: 'DistributorDivestment-ApprovalConsultantDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_ConsultantDetails_secondApproval.vue'),
                },
                {
                    name: 'DistributorDivestment-ApprovalFundConsultantDetails',
                    path: 'DistributorDivestment-ApprovalFundConsultantDetails',
                    component: () =>
                        import ('../pages/distributor-management/distributor/divestment/cm1_distributorDivestment_FundConsultantDetails_secondApproval.vue'),
                },
                // end divestment

                // Extension Request
                {
                    name: 'extension-request',
                    path: 'extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorExtensionRequestList.vue')
                },
                {
                    name: 'extension-request-history',
                    path: 'extension-request-history/:id',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorExtensionRequestHistoryList.vue')
                },
                {
                    name: 'create-extension-request',
                    path: 'create-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorExtensionRequest.vue')
                },
                {
                    name: 'view-extension-request',
                    path: 'view-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_view_extension_request.vue')
                },
                {
                    name: 'extension-request-update',
                    path: 'extension-request-update',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributor_admin_update_extension_request.vue')
                },
                {
                    name: 'extension-request-admin-correction',
                    path: 'extension-request-admin-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributor_admin_correction.vue')
                },
                {
                    name: 'extension-request-manager-approval',
                    path: 'extension-request-manager-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributor_manager_approval.vue')
                },
                {
                    name: 'extension-request-manager-correction',
                    path: 'extension-request-manager-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/cm1_distributor_manager_correction.vue')
                },
                {
                    name: 'extension-request-action',
                    path: 'extension-request/:action',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorViewExtensionRequest.vue')
                },
                {
                    name: 'subsequent-extension-request',
                    path: 'subsequent-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorSubsequentExtensionRequestList.vue')
                },
                {
                    name: 'subsequent-extension-request-approval-list',
                    path: 'subsequent-extension-request-approval-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributorSubsequentExtensionRequestApprovalList.vue')
                },
                {
                    name: 'view-subsequent-extension-request',
                    path: 'view-subsequent-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_view_subsequent_extension_request.vue')
                },
                {
                    name: 'subsequent-extension-request-manager-approval',
                    path: 'subsequent-extension-request-manager-approval',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_manager_approval.vue')
                },
                {
                    name: 'subsequent-extension-request-manager-correction',
                    path: 'subsequent-extension-request-manager-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_manager_correction.vue')
                },
                {
                    name: 'subsequent-extension-request-manager-update',
                    path: 'subsequent-extension-request-manager-update',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_manager_update.vue')
                },
                {
                    name: 'create-subsequent-extension-request',
                    path: 'create-subsequent-extension-request',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_admin_create.vue')
                },
                {
                    name: 'subsequent-extension-request-admin-update',
                    path: 'subsequent-extension-request-admin-update',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_admin_update.vue')
                },
                {
                    name: 'subsequent-extension-request-admin-correction',
                    path: 'subsequent-extension-request-admin-correction',
                    component: () =>
                        import ('../pages/distributor-management/distributor/extension-request/subsequent/cm1_distributor_admin_correction.vue')
                },
                {
                    name: 'subsequent-extension-request-action',
                    path: 'subsequent-extension-request/:action',
                    component: () =>
                        import ('../pages/distributor-management/distributor/cm1_distributorViewSubsequentExtensionRequest.vue')
                },
                // End Extension Request

                //DISTRIBUTION POINT
                {
                    name: 'distribution-point-list',
                    path: 'distribution-point-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/distribution-point/cm1_distributionPoint.vue')
                },
                {
                    name: 'register-distributionPoint',
                    path: 'register-distributionPoint',
                    component: () =>
                        import ('../pages/distributor-management/distributor/distribution-point/cm1_new_distributionPoint.vue')
                },
                {
                    name: 'edit-distributionPoint',
                    path: 'edit-distributionPoint',
                    component: () =>
                        import ('../pages/distributor-management/distributor/distribution-point/cm1_editDistributionPoint.vue')
                },
                {
                    name: 'view-distributionPoint',
                    path: 'view-distributionPoint',
                    component: () =>
                        import ('../pages/distributor-management/distributor/distribution-point/cm1_viewDistributionPoint.vue')
                },

                //DISTRIBUTOR USER -acap
                {
                    name: 'distributor-user-profile-set-up',
                    path: 'distributor-user-profile-set-up',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userProfile.vue')
                },
                {
                    name: 'distributor-user-management-list',
                    path: 'distributor-user-management-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userManagementList.vue')
                },
                {
                    name: 'distributor-user-management-review',
                    path: 'distributor-user-management-review',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userManagementReview.vue')
                },
                {
                    name: 'distributor-user-role-application',
                    path: 'distributor-user-role-application',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userRole.vue')
                },
                {
                    name: 'distributor-user-role-list',
                    path: 'distributor-user-role-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userRoleApplicationList.vue')
                },
                {
                    name: 'distributor-user-role-review',
                    path: 'distributor-user-role-review',
                    component: () =>
                        import ('../pages/distributor-management/distributor/user-management/cm1_distributor_userRoleApplicationReview.vue')
                },
                //SUSPEND-REVOCATION APPEAL - NURUL
                {
                    name: 'suspension-revocation-notification-list',
                    path: 'suspension-revocation-notification-list',
                    component: () =>
                        import ('../pages/distributor-management/distributor/suspension_revocation/cm1_suspension_revocation_notificationList.vue')
                },

                {
                    name: 'view-suspension-revocation-form',
                    path: 'view-suspension-revocation-form',
                    component: () =>
                        import ('../pages/distributor-management/distributor/suspension_revocation/cm1_view_suspension_revocation_record.vue')
                },

                {
                    name: 'update-suspension-revocation-form',
                    path: 'update-suspension-revocation-form',
                    component: () =>
                        import ('../pages/distributor-management/distributor/suspension_revocation/cm1_update_suspension_revocation_form.vue')
                },
                //SUSPEND-REVOCATION APPEAL - NURUL - END



            ],
            beforeEnter: ifAuthenticatedDistributor,
        },
        /* ---------------THIRD PARTY PAGES--------------------- */
        {
            name: 'Third Party',
            path: '/third-party',
            component: AppLayout,
            meta: {
                progress: {
                    func: [
                        { call: 'color', modifier: 'temp', argument: '#ffb000' },
                        { call: 'fail', modifier: 'temp', argument: '#6e0000' },
                        { call: 'location', modifier: 'temp', argument: 'top' },
                        {
                            call: 'transition',
                            modifier: 'temp',
                            argument: { speed: '1.5s', opacity: '0.6s', termination: 400 },
                        },
                    ],
                },
            },
            children: [{
                    name: 'others-dashboard',
                    path: 'dashboard',
                    component: () =>
                        import ('../components/dashboard/Dashboard.vue'),
                    // default: true,
                },
                {
                    name: 'announcement',
                    path: 'announcement',
                    component: () =>
                        import ('../pages/admin-configuration/announcement/index.vue'),
                    props: { role: 'third-party' },
                },
                {
                    name: 'pending-task',
                    path: 'pending-task',
                    component: () =>
                        import ('../pages/admin-configuration/pending-task/index.vue'),
                    props: { role: 'third-party' },
                },
                {
                    name: 'notification',
                    path: 'notification',
                    component: () =>
                        import ('../pages/admin-configuration/notification/index.vue'),
                    props: { role: 'third-party' },
                },
                {
                    name: 'profile',
                    path: 'profile',
                    component: () =>
                        import ('../pages/admin-configuration/profile/index.vue'),
                    props: { role: 'third-party' },
                },
                // NAV Daily Publish Start
                {
                    name: 'nav-list',
                    path: 'nav-list',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdParty_NAVList.vue'),
                    props: { role: 'third-party' },
                },
                // NAV Daily Publish End
                // 3rd Party Company Registration Start
                {
                    name: 'company-registration',
                    path: 'company-registration',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdparty_CompanyRegistration.vue'),
                    props: { role: 'third-party' },
                },
                // 3rd Party Company Registration End

                //3rd Party Fund Creation Start
                {
                    name: 'fund-list',
                    path: 'fund-list',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdparty_fundCreationList.vue'),
                    // props: { role: 'third-party' },
                },
                {
                    name: 'fund-creation',
                    path: 'fund-creation',
                    component: () =>
                        import ('../pages/funds-malaysia/thirdparty/cm5_thirdparty_fundCreation.vue'),
                    // props: { role: 'third-party' },
                },
            ],
            beforeEnter: ifAuthenticatedThirdParty,
        },
        /* --------------- MEDIA ------------------------------------- */
        {
            name: 'fms_website',
            path: '/fms_website',
            component: fmspageloggedin,
            props: true,
        },
        /* ---------------TRAINING PROVIDER PAGES--------------------- */
        {
            name: 'Training Provider',
            path: '/training-provider',
            component: AppLayoutconsultant,
            children: [{
                    name: 'trp-dashboard',
                    path: 'dashboard',
                    component: () =>
                        import ('../components/dashboard/Dashboard.vue'),
                    // default: true,
                },

                {
                    name: 'trp-register',
                    path: 'trp-register',
                    component: () =>
                        import ('../pages/funds-malaysia/training_provider/trp_register.vue'),
                    props: true,
                },
                {
                    name: 'announcement',
                    path: 'announcement',
                    component: () =>
                        import ('../pages/admin-configuration/announcement/index.vue'),
                    props: { role: 'training-provider' },
                },
                {
                    name: 'pending-task',
                    path: 'pending-task',
                    component: () =>
                        import ('../pages/admin-configuration/pending-task/index.vue'),
                    props: { role: 'training-provider' },
                },
                {
                    name: 'notification',
                    path: 'notification',
                    component: () =>
                        import ('../pages/admin-configuration/notification/index.vue'),
                    props: { role: 'training-provider' },
                },
                {
                    name: 'profile',
                    path: 'profile',
                    component: () =>
                        import ('../pages/admin-configuration/profile/index.vue'),
                    props: { role: 'training-provider' },
                },
                // CPD
                // TRP - PostVetting
                {
                    name: 't-preVettingProgramList',
                    path: 't-preVettingProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/trp/preVetting/cm04_tPreVetting_programList.vue'
                        ),
                },
                {
                    name: 't-preVettingNewSubmission',
                    path: 't-preVettingNewSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/trp/preVetting/cm04_tPreVetting_newSubmission.vue'
                        ),
                },
                {
                    name: 't-preVettingRepeatSubmission',
                    path: 't-preVettingRepeatSubmission',
                    component: () =>
                        import (
                            '../pages/continuing-development/trp/preVetting/cm04_tPreVetting_repeatSubmission.vue'
                        ),
                },
                {
                    name: 't-preVettingViewProgramList',
                    path: 't-preVettingViewProgramList',
                    component: () =>
                        import (
                            '../pages/continuing-development/trp/preVetting/cm04_tPreVetting_viewProgramList.vue'
                        ),
                },
                // end CPD
            ],
            beforeEnter: ifAuthenticatedTrainingProvider,
        },
        ...ConsultantRoutes
    ],
});