import * as ConsultantAPI from "./api_client";
import * as data_exams_preferences from "../stubs/exams_preferences.json"
import * as data_exams_sites from "../stubs/exams_sites.json"
import * as data_exams_states from "../stubs/exams_states.json"
import * as data_exams_exemption from "../stubs/exams_exemption.json"

let API = ConsultantAPI.get_client();

const IS_STUB = false;

/*
GET	api/module2/exams
GET	api/module2/exams/courses
GET	api/module2/exams/sites
GET	api/module2/exams/preferences

POST	api/module2/exams
*/

/*
params:
"CONSULTANT_EXAM_ID:1 || CONSULTANT_ID:26 || APPLICATION_ID:1001"
*/
export async function get_exams() {
  return API.get("/exams")
    .then(response => {
      return response.data;
    })
    .catch(function(error) {
      throw error;
    });
}

export async function get_exams_center(state) {
  let data = {
    STATE: state
  }
  if(IS_STUB){
    return data_exams_sites.default
  }
  return API.get("/exams/sites", {
    params: data
  })
    .then(response => {
      return response.data;
    })
    .catch(function(error) {
      throw error;
    });
}

export async function get_exams_exemption() {
  if(IS_STUB){
    return data_exams_exemption.default
  }
  return API.get("/exemption-categories")
    .then(response => {
      return response.data;
    })
    .catch(function(error) {
      throw error;
    });
}

export async function get_exams_states() {
  if(IS_STUB){
    return data_exams_states.default
  }
  return API.get("/exams/sites/states")
    .then(response => {
      return response.data;
    })
    .catch(function(error) {
      throw error;
    });
}

/*
params:
"CONSULTANT_LICENSE_ID:1
SITEID:ASLC
STARTFROM:13/09/2021"
*/
export async function get_exams_preferences(consultant_license_id, site_id, start_from) {
  let data = {
    CONSULTANT_LICENSE_ID: consultant_license_id,
    SITEID: site_id,
    STARTFROM: start_from
  }
  if(IS_STUB){
    return data_exams_preferences.default
  }
  return API.get("/exams/preferences", {
    params: data
  })
    .then(response => {
      return response.data;
    })
    .catch(function(error) {
      throw error;
    });
}
