import axios from "axios";
const API = axios.create({
  baseURL: "/api/module2"
});

window.API_CONSULTANT = API;

export function get_client(){
  console.log(API.defaults.data)
  return API;
}

export function update_client(){
  return API;
}

export function add_header() {
  const access_token = localStorage.getItem("access_token");
  const refresh_token = localStorage.getItem("refresh_token");
  const token_data = localStorage.getItem("token_data");

  if (access_token) {
    console.log("Token exist: ", access_token);
    API.defaults.headers.common["Authorization"] = "Bearer " + access_token;
  }
  if(token_data){
    API.defaults.data = JSON.parse(token_data);
  }
  return API;
}

export function remove_header() {
  localStorage.removeItem("access_token");
  localStorage.removeItem("refresh_token");
  localStorage.removeItem("token_data");

  delete API.defaults.headers.common["Authorization"];

  return API;
}

export function update_api_client(data) {
  const access_token = data.access_token;
  const refresh_token = data.refresh_token;

  localStorage.setItem("access_token", access_token);
  localStorage.setItem("refresh_token", refresh_token);
  localStorage.setItem("token_data", JSON.stringify(data));

  API.defaults.data = data;
  API.defaults.headers.common["Authorization"] = "Bearer " + access_token;
  return API;
}
