const path = require('path');
const StylelintPlugin = require('stylelint-webpack-plugin');

const lintOnSave = false;

module.exports = {
    lintOnSave: false,
    transpileDependencies: [
        'vuestic-ui',
        'epic-spinners',
    ],
    pages: {
        index: {
            // entry for the page
            entry: 'src/app/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            filename: 'index.html',
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'FiMM Admin',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    },
    configureWebpack: {
        resolve: {
            alias: {
                vue$: 'vue/dist/vue.esm.js',
                '@': path.resolve('src'),
            },
        },
        plugins: [
            ...(
                (!lintOnSave && process.env.NODE_ENV === 'development') ? [] : [new StylelintPlugin({
                    files: ['src/**/*.{vue,htm,html,css,sss,less,scss}'],
                })]
            ),
        ],

        devServer: {
            disableHostCheck: true,
            port: process.env.VUE_APP_PORT || 8080,
            proxy: {
                // '^/api/koolreport': {
                //   target: 'http://localhost:7008',
                //   ws: true,
                //   changeOrigin: true,
                //   secure: false,
                // },
                // public: 'lfcs-stg.fimm.com.my',
                // disableHostCheck: true,
                '^/api/module0': {
                    target: process.env.MODULE0_URL || 'http://localhost:7000',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module1': {
                    target: process.env.MODULE1_URL || 'http://localhost:7001',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module2': {
                    target: process.env.MODULE2_URL || 'http://localhost:7002',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module3': {
                    target: process.env.MODULE3_URL || 'http://localhost:7003',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module4': {
                    target: process.env.MODULE4_URL || 'http://localhost:7004',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module5': {
                    target: process.env.MODULE5_URL || 'http://localhost:7005',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module6': {
                    target: process.env.MODULE6_URL || 'http://localhost:7006',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                '^/api/module7': {
                    target: process.env.MODULE7_URL || 'http://localhost:7007',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                },
                'api/ems': {
                    target: process.env.EMS_URL || 'https://ems-stg.fimm.com.my/api',
                    ws: true,
                    changeOrigin: true,
                    secure: false,
                }
            },
        },
    },
    css: {
        loaderOptions: {
            sass: {
                // Preload vuestic-ui variables and mixins for every component
                data: '@import "~vuestic-ui/src/components/vuestic-sass/resources/resources.scss";',
            },
        },
    },
    pwa: {
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: './src/service-worker.js',
            importWorkboxFrom: 'local',
        },
    },
};
